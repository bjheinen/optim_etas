[run_options]
verbose = 0
save_output_data = 1
results_file = 22-08-16_092018
opt_switch = on
record_config = 1
output_file = RESULTS/22-08-16_092018.csv
random_init_params = 0
results_folder = RESULTS
log_param = 0
op_code = L-BFGS-B
method = analyt

[main_param]
start_learning = 1985
start_time = 727198.0
mc_target = 3.95
test_boundary_dist = 1
end_time = 734503.0
mc_learning = 3.5

[input_settings]
spatial_test_bins = INPUT/SpatialTestBins.npy
b = 1.0
collection_area = INPUT/RELMCollectionArea.npy
spatial_dens_bg = INPUT/CIF_conan_m2M4_km2.npy
input_catalog = INPUT/input_catalog.npy
param_file = INPUT/param_original_gs_none.npy

[anis_param]
max_dist_anis = 3.0
smoothing_sigma = 2.0
anis_d = 2.0
start_ea_collect = 0.0416666666667
min_mag_anis = 5.5
anis_q = 1.5
end_ea_collect = 2.0
max_dist_ea_collect = 2.0

[additional_info]
number_of_learning_events = 5388
number_of_targets = 1389

