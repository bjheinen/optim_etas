========================================
0:00:00.000 - Start Program
========================================
========================================
0:00:00.813 - Split - Set-up Complete
========================================

 ======================================== 

Optimisation Finished ... ...
ll_f  =   -14436.9154212
ll_bg =   -20125.3759611 

Final Results for: 
          
          gs : original : EAS 
          
          Expected N_TI     =  1413.4408275 
          Expected N_TI_2   =  1389.0 
          Observed Nt       =  1389 
          
          Probability Gain  =  60.0611950296 
          Information Gain  =  4.09536395962 

========================================
Optimisation routine information: 
   status: 0
  success: True
     nfev: 427
 hess_inv: <6x6 LbfgsInvHessProduct with dtype=float64>
      fun: 14436.915421226891
        x: array([  5.71495439e-02,   6.29748595e-01,   7.56348136e-01,
         1.02256022e+00,   2.37725464e-03,   2.58611809e+00])
  message: 'CONVERGENCE: REL_REDUCTION_OF_F_<=_FACTR*EPSMCH'
      jac: array([ -1.55741873e+00,  -1.04955689e-01,  -3.27418093e-03,
        -1.63636287e+00,  -3.23852873e+00,  -7.27595761e-04])
      nit: 44
========================================
Using information gain: 
AIC  =   3.80927208076
AICc =   3.87005355688
BIC  =   35.2273081373 

Using ll: 
AIC  =   28885.8308425
AICc =   28885.8916239
BIC  =   28917.2488785
========================================
Inverse Hessian Matrix: 

[[  3.81721154e+00  -2.31146142e+00   8.64707910e-01  -2.63738762e-01
   -9.69152081e-02  -6.30238448e+01]
 [ -2.31146142e+00   8.49114562e+01  -9.12953526e+00  -3.71938174e+00
   -1.40581171e+00   2.06602701e+02]
 [  8.64707910e-01  -9.12953526e+00   2.06353180e+00   3.83203093e-01
    2.58694470e-01  -2.68585545e+01]
 [ -2.63738762e-01  -3.71938174e+00   3.83203093e-01   2.59375817e-01
    1.42026904e-01  -3.02053998e+00]
 [ -9.69152081e-02  -1.40581171e+00   2.58694470e-01   1.42026904e-01
    1.09526988e-01  -4.75629861e-01]
 [ -6.30238448e+01   2.06602701e+02  -2.68585545e+01  -3.02053998e+00
   -4.75629861e-01   1.40633755e+03]] 

========================================
0:06:19.613 - Split
0:06:18.799 - Lap
========================================

Saving optimised parameters ... 
Saving parameter uncertainties from inverse hessian ...
Saving output data ...
Results saved to ./RESULTS 


 ======================================== 

Optimisation Finished ... ...
ll_f  =   -14152.3743061
ll_bg =   -20125.3759611 

Final Results for: 
          
          gs_sc_Ogata1 : original : EAS 
          
          Expected N_TI     =  1413.4408275 
          Expected N_TI_2   =  1389.0 
          Observed Nt       =  1389 
          
          Probability Gain  =  73.7158011364 
          Information Gain  =  4.30021717427 

========================================
Optimisation routine information: 
   status: 0
  success: True
     nfev: 266
 hess_inv: <6x6 LbfgsInvHessProduct with dtype=float64>
      fun: 14152.374306082625
        x: array([ 0.05989451,  0.29465686,  0.84706902,  1.03856951,  0.00331304,
        0.4987753 ])
  message: 'CONVERGENCE: REL_REDUCTION_OF_F_<=_FACTR*EPSMCH'
      jac: array([ 1.10067049,  0.0165528 , -0.60226739, -0.03838068, -0.16389095,
        0.06548362])
      nit: 25
========================================
Using information gain: 
AIC  =   3.39956565147
AICc =   3.46034712759
BIC  =   34.817601708 

Using ll: 
AIC  =   28316.7486122
AICc =   28316.8093936
BIC  =   28348.1666482
========================================
Inverse Hessian Matrix: 

[[  7.92309078e-01  -1.10947588e+00  -1.76620868e-01   1.64046813e-01
   -2.24539782e-02  -2.86153477e+00]
 [ -1.10947588e+00   1.08116302e+01   7.90955277e-01  -2.31021700e+00
   -1.75936053e-03   3.67400171e+01]
 [ -1.76620868e-01   7.90955277e-01   9.26050380e-01  -3.21096815e-01
    1.02073422e-03   4.36159781e+00]
 [  1.64046813e-01  -2.31021700e+00  -3.21096815e-01   5.32358294e-01
    3.19978626e-03  -8.29101994e+00]
 [ -2.24539782e-02  -1.75936053e-03   1.02073422e-03   3.19978626e-03
    7.60710941e-04  -4.05850191e-02]
 [ -2.86153477e+00   3.67400171e+01   4.36159781e+00  -8.29101994e+00
   -4.05850191e-02   1.29903123e+02]] 

========================================
0:10:17.294 - Split
0:03:57.681 - Lap
========================================

Saving optimised parameters ... 
Saving parameter uncertainties from inverse hessian ...
Saving output data ...
Results saved to ./RESULTS 


 ======================================== 

Optimisation Finished ... ...
ll_f  =   -14147.6748805
ll_bg =   -20125.3759611 

Final Results for: 
          
          gs_sc_WHJK : original : EAS 
          
          Expected N_TI     =  1413.4408275 
          Expected N_TI_2   =  1389.0 
          Observed Nt       =  1389 
          
          Probability Gain  =  73.9656273504 
          Information Gain  =  4.30360049003 

========================================
Optimisation routine information: 
   status: 0
  success: True
     nfev: 357
 hess_inv: <6x6 LbfgsInvHessProduct with dtype=float64>
      fun: 14147.674880491102
        x: array([ 0.05978217,  0.29939187,  0.8406834 ,  1.0390809 ,  0.00331615,
        0.42076481])
  message: 'CONVERGENCE: REL_REDUCTION_OF_F_<=_FACTR*EPSMCH'
      jac: array([-2.02981028,  0.34051482, -0.27048372,  2.27682904,  5.32527338,
        0.2230081 ])
      nit: 35
========================================
Using information gain: 
AIC  =   3.39279901994
AICc =   3.45358049607
BIC  =   34.8108350765 

Using ll: 
AIC  =   28307.349761
AICc =   28307.4105425
BIC  =   28338.767797
========================================
Inverse Hessian Matrix: 

[[  3.08024815e+00   4.09353502e+01   5.67561660e+00  -7.60744245e+00
    1.07875025e-02   2.11307462e+01]
 [  4.09353502e+01   6.00793588e+02   8.29324626e+01  -1.11562204e+02
    5.96263040e-01   3.31633660e+02]
 [  5.67561660e+00   8.29324626e+01   1.21119267e+01  -1.55502490e+01
    6.82815475e-02   4.62663274e+01]
 [ -7.60744245e+00  -1.11562204e+02  -1.55502490e+01   2.07879488e+01
   -1.09599048e-01  -6.17000533e+01]
 [  1.07875025e-02   5.96263040e-01   6.82815475e-02  -1.09599048e-01
    3.99102948e-03   4.66270836e-01]
 [  2.11307462e+01   3.31633660e+02   4.62663274e+01  -6.17000533e+01
    4.66270836e-01   1.91401424e+02]] 

========================================
0:15:38.322 - Split
0:05:21.028 - Lap
========================================

Saving optimised parameters ... 
Saving parameter uncertainties from inverse hessian ...
Saving output data ...
Results saved to ./RESULTS 


 ======================================== 

Optimisation Finished ... ...
ll_f  =   -14148.3773919
ll_bg =   -20125.3759611 

Final Results for: 
          
          gs_sc_Kagan : original : EAS 
          
          Expected N_TI     =  1413.4408275 
          Expected N_TI_2   =  1389.0 
          Observed Nt       =  1389 
          
          Probability Gain  =  73.9282273815 
          Information Gain  =  4.3030947223 

========================================
Optimisation routine information: 
   status: 0
  success: True
     nfev: 1024
 hess_inv: <7x7 LbfgsInvHessProduct with dtype=float64>
      fun: 14148.377391869893
        x: array([ 0.05976509,  0.30374073,  0.83818856,  1.0388077 ,  0.00328987,
        0.97043782,  0.44641484])
  message: 'CONVERGENCE: REL_REDUCTION_OF_F_<=_FACTR*EPSMCH'
      jac: array([ 1.10467226, -0.89603418, -1.09630491, -7.17718649, -3.24325811,
       -0.16516424,  0.23246685])
      nit: 81
========================================
Using information gain: 
AIC  =   5.39381055541
AICc =   5.47491120711
BIC  =   42.0481859547 

Using ll: 
AIC  =   28310.7547837
AICc =   28310.8358844
BIC  =   28347.4091591
========================================
Inverse Hessian Matrix: 

[[   0.71209507    9.46711568   -2.82656967   -0.3636952     0.7717826
    -6.53524706    2.83438153]
 [   9.46711568  138.26008084  -41.09066456   -6.03104806   10.4753619
  -113.63451229   46.57241402]
 [  -2.82656967  -41.09066456   12.36592599    1.74008389   -3.13499526
    34.45385664  -14.1798304 ]
 [  -0.3636952    -6.03104806    1.74008389    0.31298145   -0.41186258
     5.65768075   -2.19039606]
 [   0.7717826    10.4753619    -3.13499526   -0.41186258    0.84102246
    -7.6092218     3.25437505]
 [  -6.53524706 -113.63451229   34.45385664    5.65768075   -7.6092218
   123.64821806  -47.76741003]
 [   2.83438153   46.57241402  -14.1798304    -2.19039606    3.25437505
   -47.76741003   18.77953033]] 

========================================
0:30:58.068 - Split
0:15:19.745 - Lap
========================================

Saving optimised parameters ... 
Saving parameter uncertainties from inverse hessian ...
Saving output data ...
Results saved to ./RESULTS 


 ======================================== 

Optimisation Finished ... ...
ll_f  =   -14151.2585819
ll_bg =   -20125.3759611 

Final Results for: 
          
          pl : original : EAS 
          
          Expected N_TI     =  1413.4408275 
          Expected N_TI_2   =  1389.0 
          Observed Nt       =  1389 
          
          Probability Gain  =  73.7750376675 
          Information Gain  =  4.3010204314 

========================================
Optimisation routine information: 
   status: 0
  success: True
     nfev: 400
 hess_inv: <7x7 LbfgsInvHessProduct with dtype=float64>
      fun: 14151.258581922275
        x: array([ 0.05323173,  0.44920611,  0.78507874,  1.03163702,  0.0027409 ,
        0.99239022,  1.45650953])
  message: 'CONVERGENCE: REL_REDUCTION_OF_F_<=_FACTR*EPSMCH'
      jac: array([ 2.50784069, -2.01252988, -1.48993422, -4.53983375,  0.07657945,
       -0.48257789, -8.04357114])
      nit: 34
========================================
Using information gain: 
AIC  =   5.3979591372
AICc =   5.4790597889
BIC  =   42.0523345365 

Using ll: 
AIC  =   28316.5171638
AICc =   28316.5982645
BIC  =   28353.1715392
========================================
Inverse Hessian Matrix: 

[[  1.01343813e+00  -1.44939453e+01   9.73754144e+00  -2.32302966e-01
   -8.94909664e-02  -9.08506559e+00   2.13610978e+00]
 [ -1.44939453e+01   4.26022969e+02  -2.41813029e+02  -1.45016450e+00
    1.45105178e+00   1.88324046e+02  -6.29948315e+01]
 [  9.73754144e+00  -2.41813029e+02   1.90599387e+02  -7.63199590e+00
   -1.69142870e+00  -1.45151271e+02   3.20255867e+01]
 [ -2.32302966e-01  -1.45016450e+00  -7.63199590e+00   1.34894857e+00
    1.33988117e-01   5.55009531e+00   7.86349345e-01]
 [ -8.94909664e-02   1.45105178e+00  -1.69142870e+00   1.33988117e-01
    1.95968958e-02   1.32032834e+00  -1.55375105e-01]
 [ -9.08506559e+00   1.88324046e+02  -1.45151271e+02   5.55009531e+00
    1.32032834e+00   1.18113183e+02  -2.41795370e+01]
 [  2.13610978e+00  -6.29948315e+01   3.20255867e+01   7.86349345e-01
   -1.55375105e-01  -2.41795370e+01   1.04971219e+01]] 

========================================
0:36:55.586 - Split
0:05:57.518 - Lap
========================================

Saving optimised parameters ... 
Saving parameter uncertainties from inverse hessian ...
Saving output data ...
Results saved to ./RESULTS 


 ======================================== 

Optimisation Finished ... ...
ll_f  =   -14053.3247894
ll_bg =   -20125.3759611 

Final Results for: 
          
          pl_sc_Ogata : original : EAS 
          
          Expected N_TI     =  1413.4408275 
          Expected N_TI_2   =  1389.0 
          Observed Nt       =  1389 
          
          Probability Gain  =  79.1644331419 
          Information Gain  =  4.37152712146 

========================================
Optimisation routine information: 
   status: 0
  success: True
     nfev: 368
 hess_inv: <7x7 LbfgsInvHessProduct with dtype=float64>
      fun: 14053.324789425395
        x: array([ 0.05540745,  0.34943094,  0.82711131,  1.03551929,  0.00316026,
        0.38127151,  1.78634136])
  message: 'CONVERGENCE: REL_REDUCTION_OF_F_<=_FACTR*EPSMCH'
      jac: array([ 0.0851287 , -0.47402864,  0.58207661,  0.99662429,  5.92117431,
        3.14248609,  1.63363438])
      nit: 34
========================================
Using information gain: 
AIC  =   5.25694575707
AICc =   5.33804640877
BIC  =   41.9113211564 

Using ll: 
AIC  =   28120.6495789
AICc =   28120.7306795
BIC  =   28157.3039543
========================================
Inverse Hessian Matrix: 

[[  0.9329265   -4.112057    -2.00667397   0.61198522  -0.0783103
   -1.41785408   1.18565826]
 [ -4.112057    21.94446961   1.09851777  -2.65521707   0.0427598
    9.38577123  -9.28200355]
 [ -2.00667397   1.09851777  23.19873045  -1.7254067    0.80632366
   -4.474008     6.5717227 ]
 [  0.61198522  -2.65521707  -1.7254067    0.43933295  -0.05199913
   -0.83678094   0.70755237]
 [ -0.0783103    0.0427598    0.80632366  -0.05199913   0.03765513
   -0.22652626   0.32879402]
 [ -1.41785408   9.38577123  -4.474008    -0.83678094  -0.22652626
    6.74154275  -6.36314921]
 [  1.18565826  -9.28200355   6.5717227    0.70755237   0.32879402
   -6.36314921   8.28973397]] 

========================================
0:42:26.610 - Split
0:05:31.023 - Lap
========================================

Saving optimised parameters ... 
Saving parameter uncertainties from inverse hessian ...
Saving output data ...
Results saved to ./RESULTS 


 ======================================== 

Optimisation Finished ... ...
ll_f  =   -14062.8026073
ll_bg =   -20125.3759611 

Final Results for: 
          
          pl_sc_WHJK : original : EAS 
          
          Expected N_TI     =  1413.4408275 
          Expected N_TI_2   =  1389.0 
          Observed Nt       =  1389 
          
          Probability Gain  =  78.6260947389 
          Information Gain  =  4.36470363847 

========================================
Optimisation routine information: 
   status: 0
  success: True
     nfev: 420
 hess_inv: <6x6 LbfgsInvHessProduct with dtype=float64>
      fun: 14062.802607302559
        x: array([ 0.05341787,  0.39951988,  0.81632274,  1.03199482,  0.00302163,
        0.19394732])
  message: 'CONVERGENCE: REL_REDUCTION_OF_F_<=_FACTR*EPSMCH'
      jac: array([ 0.23101165, -0.04074536, -0.03165042, -0.26084308,  1.8018909 ,
        0.09658834])
      nit: 43
========================================
Using information gain: 
AIC  =   3.27059272306
AICc =   3.33137419918
BIC  =   34.6886287796 

Using ll: 
AIC  =   28137.6052146
AICc =   28137.6659961
BIC  =   28169.0232507
========================================
Inverse Hessian Matrix: 

[[  5.33274498e-02   6.80260333e-01  -1.12184157e-01  -5.89919121e-02
   -3.97168961e-03   2.30254313e-01]
 [  6.80260333e-01   2.14521568e+02  -4.83861163e+01  -1.48293508e+01
   -1.42262370e+00   8.56128410e+01]
 [ -1.12184157e-01  -4.83861163e+01   1.47238802e+01   2.76181284e+00
    3.31304558e-01  -2.17687362e+01]
 [ -5.89919121e-02  -1.48293508e+01   2.76181284e+00   1.11496517e+00
    9.67206448e-02  -5.54060077e+00]
 [ -3.97168961e-03  -1.42262370e+00   3.31304558e-01   9.67206448e-02
    9.50828924e-03  -5.75021835e-01]
 [  2.30254313e-01   8.56128410e+01  -2.17687362e+01  -5.54060077e+00
   -5.75021835e-01   3.57627064e+01]] 

========================================
0:48:47.682 - Split
0:06:21.072 - Lap
========================================

Saving optimised parameters ... 
Saving parameter uncertainties from inverse hessian ...
Saving output data ...
Results saved to ./RESULTS 


 ======================================== 

Optimisation Finished ... ...
ll_f  =   -14053.6369991
ll_bg =   -20125.3759611 

Final Results for: 
          
          pl_sc_Zhuang : original : EAS 
          
          Expected N_TI     =  1413.4408275 
          Expected N_TI_2   =  1389.0 
          Observed Nt       =  1389 
          
          Probability Gain  =  79.1466411141 
          Information Gain  =  4.37130234846 

========================================
Optimisation routine information: 
   status: 0
  success: True
     nfev: 576
 hess_inv: <8x8 LbfgsInvHessProduct with dtype=float64>
      fun: 14053.636999128419
        x: array([ 0.05499191,  0.44532231,  0.8254749 ,  1.02632313,  0.00277672,
        0.47969892,  1.79382851,  1.71917288])
  message: 'CONVERGENCE: REL_REDUCTION_OF_F_<=_FACTR*EPSMCH'
      jac: array([-0.81345206,  5.4755219 ,  1.19907781,  1.53358997, -3.44662112,
        5.8702426 ,  1.49047992, -4.6180503 ])
      nit: 48
========================================
Using information gain: 
AIC  =   7.25739530308
AICc =   7.36174312917
BIC  =   49.1481100451 

Using ll: 
AIC  =   28123.2739983
AICc =   28123.3783461
BIC  =   28165.164713
========================================
Inverse Hessian Matrix: 

[[  1.03795621e-01  -5.24254209e+00  -9.26616649e-01   5.03554602e-01
   -2.16432379e-02   1.20139361e+00  -4.58851259e+00  -6.18653771e+00]
 [ -5.24254209e+00   3.65816546e+02   8.43203118e+01  -3.47909510e+01
    2.59385073e+00  -9.26231102e+01   3.28066727e+02   4.23023553e+02]
 [ -9.26616649e-01   8.43203118e+01   2.49581493e+01  -8.09271394e+00
    8.46752972e-01  -2.41610666e+01   7.83817843e+01   9.51228712e+01]
 [  5.03554602e-01  -3.47909510e+01  -8.09271394e+00   3.32860490e+00
   -2.43712295e-01   8.83796861e+00  -3.12348975e+01  -4.02212681e+01]
 [ -2.16432379e-02   2.59385073e+00   8.46752972e-01  -2.43712295e-01
    3.17192572e-02  -7.85796428e-01   2.44861736e+00   2.87903029e+00]
 [  1.20139361e+00  -9.26231102e+01  -2.41610666e+01   8.83796861e+00
   -7.85796428e-01   2.54818155e+01  -8.41919794e+01  -1.06278286e+02]
 [ -4.58851259e+00   3.28066727e+02   7.83817843e+01  -3.12348975e+01
    2.44861736e+00  -8.41919794e+01   2.96849505e+02   3.79370074e+02]
 [ -6.18653771e+00   4.23023553e+02   9.51228712e+01  -4.02212681e+01
    2.87903029e+00  -1.06278286e+02   3.79370074e+02   4.92794161e+02]] 

========================================
0:57:26.025 - Split
0:08:38.342 - Lap
========================================

Saving optimised parameters ... 
Saving parameter uncertainties from inverse hessian ...
Saving output data ...
Results saved to ./RESULTS 

========================================
0:57:26.033 - End Program
========================================
