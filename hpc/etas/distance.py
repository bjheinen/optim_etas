# -*- coding: utf-8 -*-
'''
   Calculate distance between two points on Earth

   distance.py is a useful module for calculating the distance between two
   points on the surface of Earth. 

   distance.py supports calculations of great circle distances and azimuths, 
   or the more precise Inverse Vincenty method (see distance.v_distance).

   Latitude & Longitude inputs are assumed in degrees and can be single 
   values, numpy arrays or a combination of both.

   Default output is in kilometres and is distance only. Specify nargout for
   azimuth values to be returned also.

   Functions:

   gc_distance - Calculates great circle distances assuming a sphere 
                 of radius 6371.01 km.

   v_distance  - Applies Vincenty's algorithms for geodesic distances to the 
                 Earth modelled as the using the WGS84 ellipsoid.
   
   See function docstrings for futher information/example usage.
'''
__author__ = "Benedict J Heinen"
__copyright__ = "Copyright 2015, Benedict J Heinen"
__email__ = "benedict.heinen@gmail.com"
__version__ = "0.1.0"
__license__ = "GNU GPL v3.0"
import numpy as np
np.seterr(divide='ignore', invalid='ignore')

def gc_distance(lat_1, lon_1, lat_2, lon_2, units='km', nargout=1):
    '''
    Calculates great circle distances, assuming input in degrees
    '''
    r_e = 6371.01 # in km
    pi_180 = np.pi / 180.0

    delta_lat = np.abs(lat_1 - lat_2)/pi_180
    delta_lon = np.abs(lon_1 - lon_2)/pi_180

    a = np.sin(delta_lat/2.0)
    b = np.cos(lat_1/pi_180)
    c = np.cos(lat_1/pi_180)
    d = np.sin(delta_lon/2.0)

    sin_angle = (a*a + b*c*d*d)**0.5
    central_angle = 2 * np.arcsin(sin_angle)
    distance = r_e * central_angle
    print(distance)

    if nargout == 1:
        return distance
    elif nargout == 2:
        lon_12 = (lon_1 - lon_2)/pi_180
        numerator = np.sin(lon_12)
        denominator = ((b*np.tan(lat_2/pi_180)) -
                       (np.sin(lat_1/pi_180)*np.cos(lon_12)))
        az = np.arctan2(denominator, numerator)
        print(az)
        az = np.nan_to_num(az)
        az = np.degrees(az)
        az = 360 - az
        az = (az + 360) % 360

        return distance, az

    else:
        raise ValueError('Invalid value for argument - "nargout"')


def v_distance(lat_1, lon_1, lat_2, lon_2, units='km', nargout=1):
    '''
    Returns a distance, in specified units, between two points on the Earth.
    
    The earth here is modelled as the WGS84 ellipsoid and vincenty's 
    algorithms for geodesic distances are applied.
    
    Given the coordinates of the two points (Φ1, L1) and (Φ2, L2), 
    the inverse problem finds the azimuths α1, α2 and the 
    ellipsoidal distance, s. Between two nearly antipodal points, the 
    iterative formula may fail to converge; this will occur when the initial 
    value of λ is greater than π in absolute value.

    Original paper:
        T. Vincenty, 
            'Direct and Inverse Solutions of Geodesics on the 
            Ellipsoid with application of nested equations', 
            Survey Review, vol XXIII no 176, 1975   
            http://www.ngs.noaa.gov/PUBS_LIB/inverse.pdf
            doi:10.1179/sre.1975.23.176.88
    '''
    # Define constants:
    # a - length of semi-major axis of the ellipsoid (radius at equator)
    # f - flattening of the ellipsoid
    # b - (1 - f)a length of semi-minor axis of the ellisoid (radius at poles)
    a = 6378137.0
    b = 6356752.314245
    f = 1.0 / 298.257223563
    # U - reduced latitude (lat on the auxiliary sphere)
    U_1 = np.arctan(((1-f) * np.tan(np.radians(lat_1))))    
    U_2 = np.arctan(((1-f) * np.tan(np.radians(lat_2))))
    L = np.radians(lon_1 - lon_2)
    # Terms repeatedly used in calculations computed once for efficiency
    sin_U_1 = np.sin(U_1)
    sin_U_2 = np.sin(U_2)
    cos_U_1 = np.cos(U_1)
    cos_U_2 = np.cos(U_2) 
    # iteratively evaluating the equations until lambda converges
    # difference in lambda of 1e-12 corresponds to ~ 0.06mm
    # initial estimate of lambda set to L
    l = L
    l_prev = 0
    # also set iteration limit for cases where lambda fails to converge
    iter_lim = 150
    while (np.absolute(l - l_prev) > 1e-12).any() and iter_lim > 0:
        iter_lim -= 1       
        sin_l = np.sin(l)
        cos_l = np.cos(l)
        # x * x preferred over x**2 for optimal performance
        sin_sig = np.sqrt(((cos_U_2 * sin_l)*(cos_U_2 * sin_l) + 
                           (cos_U_1*sin_U_2 - sin_U_1*cos_U_2*cos_l)*
                           (cos_U_1*sin_U_2 - sin_U_1*cos_U_2*cos_l)))                         
        cos_sig = sin_U_1*sin_U_2 + cos_U_1*cos_U_2*cos_l
        sig = np.arctan2(sin_sig, cos_sig)
        sin_a = cos_U_1 * cos_U_2 * sin_l / sin_sig
        cos_sq_a = 1 - sin_a*sin_a
        cos_2_sig_m = cos_sig - (2*sin_U_1*sin_U_2/cos_sq_a)
        c = f/16.0 * cos_sq_a * (4 + f*(4 - 3*cos_sq_a))
        l_prev = l
        # calculate next lambda (l)
        l = (L + (1 - c) * f * sin_a * 
                 (sig + c * sin_sig * 
                        (cos_2_sig_m + c * cos_sig * 
                                       (-1 + 2 * cos_2_sig_m*cos_2_sig_m))))
    if iter_lim == 0:
        raise Exception('Iteration limit exceeded - \
                         lambda failed to converge for one or more values')     

    try:    
        # evaluate using converged lambda, l    
        u_sq = cos_sq_a * (a*a - b*b)/ (b*b)
        A = 1 + u_sq/16384.0 * (4096+ u_sq * (-768 + u_sq*(320 - 175*u_sq)))
        B = u_sq/1024.0 * (256 + u_sq * (-128 + u_sq * (74 - 47*u_sq)))
        delta_sig = (B * sin_sig * (cos_2_sig_m +
                     B/4.0 * (cos_sig * (-1 + 2 * cos_2_sig_m*cos_2_sig_m) -
                     B/6.0 * cos_2_sig_m *
                     (-3 + 4*sin_sig*sin_sig) *
                     (-3 + 4 * cos_2_sig_m*cos_2_sig_m))))
        s = b * A * (sig - delta_sig)
        
        alpha_1 = np.arctan2((cos_U_2*sin_l),
                             ((cos_U_1*sin_U_2)-(sin_U_1*cos_U_2*cos_l)))
                         
        alpha_2 = np.arctan2((cos_U_1*sin_l),
                             (-(sin_U_1*cos_U_2)+(cos_U_1*sin_U_2*cos_l)))

    except UnboundLocalError:
        s = 0.0
        alpha_1 = 0.0
        alpha_2 = 0.0

    # clear nan formed from identical sets of points (L = 0)    
    s = np.nan_to_num(s)
    alpha_1 = np.nan_to_num(alpha_1)    
    
    if units == 'km':
        s /= 1000.0

    # Standardise angle to between 0-360    
    alpha_1 = np.degrees(alpha_1) 
    alpha_1 = 360 - alpha_1
    alpha_1 = (alpha_1 + 360) % 360

    if nargout == 1:    
        return s
    elif nargout == 2:
        return s, alpha_1
    #WARNING: --alpha_2 MAY NOT BE CORRECT--
    elif nargout == 3:
        return s, alpha_1, alpha_2
