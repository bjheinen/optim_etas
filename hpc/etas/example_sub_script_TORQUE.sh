#!/bin/bash
# Use above shebang line to ensure the scipt is parsed by the BASH interpreter

# request resources:

# Request 8 nodes, 16 processors each
#PBS -l nodes=8:ppn=16

# Request 10 hours of walltime to run
#PBS -l walltime=10:00:00

# Run the script from submission directory
cd $PBS_O_WORKDIR

# Run the script using python a scoop, redirect output to text file 
python -m scoop -vv etas_scoop.py > RESULTS/screen_output.txt
