# -*- coding: utf-8 -*-

# Set options for program run, all parameters, and input catalog

# Main run options:
#
# Kernel Options:
# time_kernels: ['original', 'Kagan', 'Kagan_general']
# spatial_kernels: ['gs', 'gs_sc_Ogata1', 'gs_sc_WHJK', 'gs_sc_Kagan',
#                   'pl', 'pl_sc_Ogata', 'pl_sc_WHJK', 'pl_sc_Zhuang']
# anisotropic_kernels: ['none', 'EAS']
kernel_options = {'time_kernels': ['original'],
                  'spatial_kernels': ['gs'],
                  'anisotropic_kernels': ['EAS']}

# Run settings:
# method - method used for integration - analyt, semi-num--(time, space, all)
# opt_switch - on for optimisation/parameter estimisation, if set to off a 
#              .npy file containing the ETAS model parameters must be supplied
#              (the filename can be specified in input_settings)
#              test runs the calculation on the estimated params
# op_code - choose the solver to use
# log_param - use the natural log of the parameters in the optimisation?
# random-init-params: generate random initial prms between u/l bounds?
# results_file - leave as 'auto' for time stamp format
# results_folder - name of folder, 'auto' gives time stamp
# save_output_data - boolean, save run output data & results to file?
# record_config - record this config file?
run_options = {'method': 'semi-num--space',
               'opt_switch': 'on',
               'op_code': 'L-BFGS-B',
               'log_param': 0, #TODO ERROR - REMOVE LOG OPTIONS/FUNCTIONALITY
               'random_init_params': 0,
               'results_file': 'auto',
               'results_folder': 'RESULTS',
               'save_output_data': 1,
               'record_config': 1,
               'verbose': 0
               }

# General parameters:
# start_learning - start year of learning catalog
# mc_learning - mag threshold of learning catalog
# start/end points & mag threshold of target earthquakes
# test_boundary_dist (bool) offers an option to test the distance from
# earthquake to boundary to decide whether too far to warrant spatial
# integration instead of assuming int f(x,y) = 1
# (if inside testing region) or 0 (outside)
main_param = {'start_learning': 2008,
              'mc_learning': 3.95,
              'start_time': [2011, 6, 1, 0, 0, 0, 0], #y,m,d,(h,m,s,ms)
              'end_time': [2012, 1, 1, 0, 0, 0, 0],
              'mc_target': 3.95,
              'test_boundary_dist': 1}

# Anisotropic params - specific to EAS(early aftershock smoothing) methods
# m_ea_collect - calculate anis kernels for m >= m_ea_collect
# start and end points of EA collection for smoothing use
# sigma - standard deviation of gaussian smoothing
# exp/scale - power-law exponent and scaling for smoothing of EA
anis_param = {'min_mag_anis': 5.5,
              'start_ea_collect': 1.0/24.0, #Make sure this is float (Py2.x)
              'end_ea_collect': 2.0,
              'max_dist_ea_collect': 2.0, #note units of rupture length
              'max_dist_anis': 3.0,
              'smoothing_sigma': 2.0,
              'anis_q': 1.5, # exponent for power-law smoothing
              'anis_d': 2.0} # scale for power-law smoothing

# Input settings:
# spatial_dens_bg contains the spatial density, i.e. the normalised
# probability divided by the area in km2 of each spatial cell.
# b related to this - unsure of variable usage/purpose? TODO HERE
# input_catalog contains the raw catalog data saved as .npy
# it is in zmap format
# collection_area and spatial_test_bins are .mat files contain data
# defining parameters estimated from earlier experiments
input_settings = {'input_catalog': 'INPUT/input_catalog.npy',
                  'spatial_test_bins': 'INPUT/SpatialTestBins.npy',
                  'collection_area': 'INPUT/RELMCollectionArea.npy',
                  'spatial_dens_bg': 'INPUT/CIF_conan_m2M4_km2.npy',
                  'b' : 1.0,
                  'param_file': 'INPUT/param_original_gs_none.npy'}
                  
                        
optimisation_options = {'disp': 0,
                        'maxiter': 15000,
                        'maxfun': 15000,
                        'ftol': 2.22e-8, #2.22e-9???
                        'gtol': 1e-10
                        }

##############################################################################  

import numpy as np

# TAKE CARE WHEN EDITING UPPER/LOWER BOUNDS AND INITIAL ESTIMATES
def get_param(time_kr, spat_kr, **kwargs):
    
    # Set lower and upper bounds for the optimisation
    # lb = [mu0, k, alpha] (note scipy.optimise requires type = ndarray)
    lb = np.array([1.0e-09, 1.0e-09, 0.0])
    if kwargs['log_param'] == 1:
        lb[2] = 1.0e-100
    # ub = [mu_ub, 2, b]
    # NOTE: alpha < b
    
    ub = np.array([kwargs['mu_ub'], 2.0, kwargs['b']])

    # Set initial parameters also
    param_0 = [ub[0]/4.0, 0.01, 0.8]
    
    # Set additional spatial parameter bounds & initial values for krs used
    if time_kr == 'original':
        p = 1.1
        c = 3.5e-3 # 5 minutes
        param_0 = np.append(param_0, [p, c])
        lb = np.append(lb, [1.0, 1.0e-06])
        ub = np.append(ub, [2.0, 1.0])

    elif time_kr == 'Kagan':
        p = 1.1
        c_r = 3.5e-3 # 5 minutes
        param_0 = np.append(param_0, [p, c_r])
        lb = np.append(lb, [1.0, 1.0e-06])
        ub = np.append(ub, [2.0, 1.0])

    elif time_kr == 'Kagan_general':
        c_0 = 3.5e-3 # 5 minutes
        p = 1.1
        chi = 0.5
        param_0 = np.append(param_0, [p, c_0, chi])
        lb = np.append(lb, [1.0, 1.0e-06, 1.0e-06])
        ub = np.append(ub, [2.0, 1.0, 2.0])

    else:
        raise ValueError('temporal kernel not valid')


    if spat_kr == 'gs':
        #'gs' Model 1 in Zhuang et al 2011, Rathbun 1993, Console et al 2003
        # sigma: console et al 2003 found 3.85km
        sigma = 3.85
        param_0 = np.append(param_0, [sigma])
        lb = np.append(lb, [1.0e-03])
        ub = np.append(ub, [100.0])

    elif spat_kr == 'gs_sc_Ogata1':
        # gs_sc_Ogata1 Ogata 1998, Zhuang et al. 2002:
        # scale variance by exp(alpha'(m-mc))
        # (Zhuang et al 2011 Model 2)
        # alpha_ogata: fixed to alpha of productivity law
        # alpha_ogata = math.log(10)*1
        # Zhuang et al 2011: d = 0.001 per deg^2 | 0.00004 * (111**2)
        d = 0.00004 * 12321.0 
        param_0 = np.append(param_0, [d])
        lb = np.append(lb, [1.0e-03])
        ub = np.append(ub, [100.0])

    elif spat_kr == 'gs_sc_WHJK':
        # gs_sc_WHJK after Werner et al 2011, eqn 18
        f_d = 1.0
        param_0 = np.append(param_0, [f_d])
        lb = np.append(lb, [1.0e-03])
        ub = np.append(ub, [3.0])

    elif spat_kr == 'gs_sc_Kagan':
        # after Kagan 1991, scaling the variance, BUT NOT a Rayleigh
        # distribution! Rayleigh distribution is for distances, which
        # would produce wrong units.
        epsilon = 9.5
        s_r = 0.38 #scaling
        param_0 = np.append(param_0, [epsilon, s_r])
        lb = np.append(lb, [1.0e-03, 1.0e-06])
        ub = np.append(ub, [30.0, 10.0])

    elif spat_kr == 'pl':
        #'pl' power-law kernel (unscaled: Ogata 98,
        # form 2: Console et al 2003, model 3: Zhuang et al)
        # d sqrt(0.00004 * (111**2))
        d = np.sqrt(0.00004 * 12321)
        q = 1.5
        param_0 = np.append(param_0, [d, q])
        lb = np.append(lb, [1.0e-03, 1.0])
        ub = np.append(ub, [100.0, 3.0])

    elif spat_kr == 'pl_sc_Ogata':
        #'pl_sc_Ogata' power-law kernel, scaled, Ogata 98
        # form 3 , Zhuang et al 2011
        # Model 4
        d = 0.00004 * 12321
        q = 1.5
        #alpha_ogata = math.log(10)*1
        param_0 = np.append(param_0, [d, q])
        lb = np.append(lb, [10e-06, 1.0])
        ub = np.append(ub, [100.0, 3.0])

    elif spat_kr == 'pl_sc_Zhuang':
        #'pl_sc_Zhuang' power-law kernel:
        # scaled: Ogata & Zhuang 2006, Zhuang et al 2011
        # Model 5
        # Values from Zhuang et al 2008 SoCal study.
        d = 0.00004 * 12321
        q = 1.5
        gamma = 1.16
        param_0 = np.append(param_0, [d, q, gamma])
        if kwargs['log_param'] == 1:
            lb = np.append(lb, [1.0e-03, 1.0, 1e-100])
        else:
            lb = np.append(lb, [1.0e-03, 1.0, 0.0])
        ub = np.append(ub, [100.0, 3.0, 3.0])

    elif spat_kr == 'pl_sc_WHJK':
        # power-law kernel, scaled, Werner et al, 2011
        f_d = 0.1
        param_0 = np.append(param_0, [f_d])
        if kwargs['log_param'] == 1:
            lb = np.append(lb, [1.0e-100])
        else:
            lb = np.append(lb, [0.0])
        ub = np.append(ub, [1.5])

    else:
        raise ValueError('Must specify valid spatial kernel')

    if kwargs['log_param'] == 1:      
        lb = np.log(lb)
        ub = np.log(ub)
        param_0 = np.log(param_0)

    return lb, ub, param_0
