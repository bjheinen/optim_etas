# -*- coding: utf-8 -*-
"""
   Calculate the integral terms of the log-likelihood equation 
   for ETAS-like models.
   ---   
   Method used: Analytical Integration
   
   These functions are able to integrate analytically any combination of 
   the following spatial/temporal kernels:
   
      temporal: ['original', 'Kagan', 'Kagan_general']
      spatial:  ['gs', 'gs_sc_Ogata1', 'gs_sc_WHJK', 
                 'gs_sc_Kagan', 'pl', 'pl_sc_Ogata', 
                 'pl_sc_WHJK', 'pl_sc_Zhuang']
                 
   This provides a comparison benchmark to test the feasibility/accuracy
   of numerical integration methods which can potentially be used on models
   where analytical methods fail.
"""
__author__ = "Benedict J Heinen"
__copyright__ = "Copyright 2015, Benedict J Heinen"
__email__ = "benedict.heinen@gmail.com"

import numpy as np
from matplotlib.path import Path

def integrate_time(time_kr, start_time, end_time,
                   v_time, v_mag, time_param):
    '''
    Calculates the integral of Omori's law over the period from the 
    start_time evaluation time, end_time

    If the Omori law was kicked off at times eq_times AFTER the start time S,
    then only T-vEqTimes is relevant.
    If the Omori law was started BEFORE S, then we only want to integrate
    that part of the Omori law that contributed between S and T.

    Omori law:
    phi(T-t_i) = (p-1) c^(p-1) / (T-t_i+c)^p       for T>t_i, zero otherwise.

    Omori Integral:
    PHI(T,S,t_i) = 1 - c^(p-1) / (T-t_i+c)^(p-1) for t_i>S

    PHI(T,S,t_i) = c^(p-1)/(S-t_i+c)^(p-1) - c^(p-1)/(T-t_i+c)^(p-1) for t_i<S

    Combining the first term on the right side of the equation:
    term1 = c^(p-1) / ( max(S-t_i,0)+c)^(p-1)
    '''
    p, c, chi = time_param
    if time_kr == 'Kagan':
        c = c * 10**((v_mag-4)/2)
    elif time_kr == 'Kagan_general':
        c = c * 10**(chi * (v_mag-4))
    else:
        pass
    time_int = c**(p-1) / ((start_time - v_time).clip(0) + c)**(p-1) - \
               c**(p-1) / (end_time - v_time + c)**(p-1)
    time_int = time_int.reshape(len(time_int), 1)
    return time_int


def integrate_space(v_lat, v_lon):
    '''
    Calculates the integral of the spatial kernel sKr over the test volume.
    '''
    # Assume the integral within the testing region is well approximated by one.
    spat_int = np.ones((len(v_lat)))
    # However, some of the input events in vLat, vLon might be outside the
    # testing region, and their integrals should equal zero (in this
    # approximation).

    # CA testing region:                     
    bound_points = np.array([[-125.2, 43.0], 
                             [-119.0, 43.0], [-119.0, 39.4], [-114.0, 35.7], 
                             [-113.1, 34.3], [-113.5, 32.9], [-113.6, 32.2], 
                             [-114.5, 31.7], [-117.1, 31.5], [-117.9, 31.9], 
                             [-118.4, 32.8], [-121.0, 33.7], [-121.6, 34.2], 
                             [-123.8, 37.7], [-125.4, 40.2], [-125.4, 40.5], 
                             [-125.2, 43.0]])                     
    eq_points = np.column_stack((v_lon, v_lat))
    boundary = Path(bound_points)
    # returns a bool array
    inside = boundary.contains_points(eq_points)
    spat_int = spat_int * inside
    spat_int = spat_int.reshape(len(spat_int), 1)
    return spat_int
