# -*- coding: utf-8 -*-
'''
   Time the execution of python code

   timing.py is a useful module for timing the execution of python code to
   display as in-program timestamps.

   The 'Timer' class allows timestamps to be placed at strategic points
   throughout runtime allowing the timing of bottleneck functions etc.

   The standard library time.time function is used for GNU/Linux and OSX;
   time.clock is used when run on a Windows OS

   Example syntax:

       #!usr/env/bin/python

       import timing

       # Create a timing.Timer class to start recording runtime and print
       # a start program message to the screen
       timer = timing.Timer()

       # Body of code
       .... do stuff ...
       ...
       ...

       # Call the split() method to print a current timestamp of overall
       # runtime along with an optional message (message='message')
       # and the lap time since a previous split if not the first call.
       timer.split()

       ...
       ... some expensive function ...
       ...

       timer.split()

       ...
       ...

       # Call the end() method to print an exit message and overall runtime
       timer.end()

   Future extension of this module will allow recording time of functions as
   a percentage of total time etc. Perhaps incorporate linux's excellent 'time',
   memory analysis etc...
'''
from __future__ import print_function

__author__ = "Benedict J. Heinen"
__copyright__ = "Copyright 2015, Benedict J Heinen"
__email__ = "benedict.heinen@gmail.com"
__version__ = "0.1.0"
__license__ = "GNU GPL v3.0"

from functools import reduce #needed in Py3.x
import sys
if sys.platform.startswith('win'):
    from time import clock as time
elif sys.platform.startswith('linux'):
    from time import time
elif sys.platform == 'darwin' or sys.platform.startswith('OS'):
    from time import time
else:
    from time import time


# Some border formatting for output
border_line = "="*40

class Timer():

    def __init__(self):
        '''Timer start is accompanied by a 'Start Program' message'''
        self.current_time = time()
        self.start_time = self.current_time
        self.log("Start Program")

    def format_time(self, time):
        '''Returns a string in hh:mm:ss.sss format of the time'''
        return "%d:%02d:%02d.%03d" % \
        reduce(lambda a, b: divmod(a[0], b) + a[1:],
               [(time*1000,), 1000, 60, 60])

    def update_time(self):
        '''Internal - Updates last split and calculates lap times'''
        self.current_time = time()
        try:
            self.lap_time = self.current_time - self.split_time
            self.split_time = self.current_time
        except AttributeError:
            self.split_time = self.current_time

    def log(self, state_message):
        '''Internal - Prints timing, messages and statements to the screen'''
        self.elapsed_time = self.current_time - self.start_time
        print(border_line)
        print(self.format_time(self.elapsed_time), '-', state_message)
        try:
            total_time = self.total_time
            print(border_line)
            return total_time
        except AttributeError:
            pass
        try:
            time = self.format_time(self.lap_time)
            print('%s - Lap' %time)
        except AttributeError:
            pass
        print(border_line)

    def split(self, message='Split'):
        '''
        Print a current timestamp of overall with optional message.
        Lap time since a previous call is also shown if not the first call.
        (Message deafult = 'Split')
        '''
        self.update_time()
        self.log(message)

    def end(self):
        '''Displays total run-time with an 'End Program' statement'''
        self.update_time()
        self.end_time = time()
        self.total_time = self.end_time - self.start_time
        self.log('End Program')