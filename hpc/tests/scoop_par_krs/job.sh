#!/bin/bash
# request resources:
#PBS -l nodes=8:ppn=16
#PBS -l walltime=10:00:00
cd $PBS_O_WORKDIR
python -m scoop -vv main.py > RESULTS/screen_output.txt
