#! usr/bin/env python
# -*- coding: utf-8 -*-
'''
    Constrained non-linear minimisation of log-likelihood functions from
    different variations of the ETAS model
'''
from __future__ import print_function
from past.builtins import xrange
from past.builtins import map

__author__ = "Benedict J Heinen"
__copyright__ = "Copyright 2015, Benedict J Heinen"
__email__ = "benedict.heinen@gmail.com"

# import statements 
# standard library modules first, then 3rd party packages, then author functions

# standard library (built-in modules)
try:
    import ConfigParser as configparser
except ImportError:
    import configparser
import csv
from datetime import datetime
import errno
from functools import partial
#import multiprocessing
import os
from time import strftime

# scipy stack
import numpy as np
import scipy.optimize
from matplotlib.path import Path
from matplotlib.dates import date2num

# Third party ditributed prpgramming module
from scoop import futures

# internal functions/modules
import config
import calc
import analyt
import semi_num
from distance import v_distance as calc_distance
# custom class timing.Timer used to measure run time
import timing


def cif_iso(v_lat, v_lon, v_loc_lat, v_loc_lon,
            v_m, v_dt, **kwargs):
    return np.sum(kwargs['bg_scale'] *
                  10**(kwargs['alpha']*(v_m - kwargs['mc_learning'])) *
                  kwargs['k'] *
                  calc.time_kr(kwargs['time_kr'], kwargs['p'], kwargs['c'], 
                               kwargs['chi'], v_m, v_dt) *
                  calc.spat_kr(kwargs['spat_kr'], v_lat, v_lon, v_loc_lat, 
                               v_loc_lon, v_m, kwargs['mc_learning'], 
                               kwargs['spat_param']))

def cif_parloop(i, **kwargs):
    '''
    This function evaluates the conditional intensity function (cif) of the 
    ETAS model at the times, locations & magnitudes of a set of specified 
    target earthquakes, j, in target_catalog.

    Format of target_catalog is zmap:
        lon lat decimalyr mo dy mag depth hr min sec

    To calculate the cif, a history of earthquakes is required, which is
    provided in learning_catalog (same format).

    spat_kr and time_kr specify kernel choices (spatial & temporal)

    fval_bg is used to calculate the cif of bg model
    
    cif_parloop contains the calculation required, the dict of arguments 
    passed to this function is frozen by partial function application allowing
    the function to be mapped in parallel to the correct index range. This
    is much faster than a calculation intensive for loop.
    '''
    learning_catalog = kwargs['learning_catalog']
    target_catalog = kwargs['target_catalog']
    catalog = learning_catalog[(learning_catalog[:, 2]
                                < target_catalog[i, 2]), :]
    number_of_learners = len(catalog[:, 0])
    v_lat = np.tile(target_catalog[i, 1], (number_of_learners, 1))
    v_lon = np.tile(target_catalog[i, 0], (number_of_learners, 1))
    v_loc_lat = catalog[:, 1:2]
    v_loc_lon = catalog[:, 0:1]
    v_dt = np.tile(target_catalog[i, 15], (number_of_learners, 1))
    v_times = catalog[:, 15:16]
    v_dt -= v_times
    v_m = catalog[:, 5:6]
    return cif_iso(v_lat, v_lon, v_loc_lat, v_loc_lon,
                   v_m, v_dt, **kwargs)
                   
def cif_parloop_eas(i, **kwargs):
    learning_catalog = kwargs['learning_catalog']
    target_catalog = kwargs['target_catalog']
    catalog = learning_catalog[(learning_catalog[:, 2] 
                                < target_catalog[i, 2]), :]    
    number_of_learners = len(catalog)
    v_lat = np.tile(target_catalog[i, 1], (number_of_learners, 1))
    v_lon = np.tile(target_catalog[i, 0], (number_of_learners, 1))
    v_loc_lat = catalog[:, 1:2]
    v_loc_lon = catalog[:, 0:1]
    v_dt = np.tile(target_catalog[i, 15], (number_of_learners, 1))
    # time/day differences
    v_times = catalog[:, 15:16]
    v_dt -= v_times
    v_m = catalog[:, 5:6]
        
    rupture_length = calc.rupture_length(v_m)    
    
    # calculate the rupture length of an eqk of mag M
    # assuming rupture length = 0.01 * 10^(0.5 m) [km]
    distance = calc_distance(v_lat, v_lon, v_loc_lat, v_loc_lon)    
    distance /= rupture_length
    
    # Boolean array based on conditions for evaluating anisotropically
    anis_mask =  ((v_m >= kwargs['min_mag_anis']) & 
                  (v_dt >= kwargs['start_ea_collect']) & 
                  (distance < kwargs['max_dist_anis']))
    # Check for case where no anisotropic evaluation is necessary
    if not anis_mask.any():
        return cif_iso(v_lat, v_lon, v_loc_lat, v_loc_lon,
                       v_m, v_dt, **kwargs)
    else:
        pass
     
    anis_spat_param = {'d': kwargs['anis_d'], 
                       'q': kwargs['anis_q']}                      

    v_lat_iso = v_lat[~anis_mask, np.newaxis]
    v_lon_iso = v_lon[~anis_mask, np.newaxis]
    v_m_iso = v_m[~anis_mask, np.newaxis]
    v_dt_iso = v_dt[~anis_mask, np.newaxis]
    v_loc_lat_iso = v_loc_lat[~anis_mask, np.newaxis]
    v_loc_lon_iso = v_loc_lon[~anis_mask, np.newaxis]
    
    
    cif_trig_iso = cif_iso(v_lat_iso, v_lon_iso, 
                           v_loc_lat_iso, v_loc_lon_iso,
                           v_m_iso, v_dt_iso, **kwargs)
    
    v_m_anis = v_m[anis_mask, np.newaxis]    
    v_times_anis = v_times[anis_mask, np.newaxis]
    v_loc_lat_anis = v_loc_lat[anis_mask, np.newaxis]
    v_loc_lon_anis = v_loc_lon[anis_mask, np.newaxis]
    
    spatial = np.zeros((len(v_m_anis), 1))
    
    # EA collection and smoothing
    for j in xrange(0, len(v_m_anis)):
        # Citeria for EA: must occur after large earthquake (v_m_aniso), 
        # before end_ea_collect and must be closer than 
        # max_dist_ea_collect * rupture length of earthquake
        ea_bool = ((v_times > v_times_anis[j]) &
                   (v_times < (v_times_anis[j]+kwargs['end_ea_collect'])) &
                   (calc.distance(v_loc_lat, v_loc_lon, 
                                  v_loc_lat_anis[j], v_loc_lon_anis[j])
                    <= (kwargs['max_dist_ea_collect'] * 
                        calc.rupture_length(v_m_anis[j]))))
        #spatial[j]
        spatial_j = ((1.0/(np.sum(ea_bool)+1.0)) *
                    (np.sum(calc.spat_kr('pl', v_lat[0,0], v_lon[0,0], 
                                          v_loc_lat[ea_bool, np.newaxis], 
                                          v_loc_lon[ea_bool, np.newaxis], None, None, 
                                          anis_spat_param)) +
                     calc.spat_kr(kwargs['spat_kr'], v_lat[0], v_lon[0], 
                                    v_loc_lat_anis[j], v_loc_lon_anis[j],
                                    v_m_anis[j], kwargs['mc_learning'], 
                                    kwargs['spat_param'])))

        # BUGFIX - spatial_j sometimes returned as single value 
	# 	   > only appears with larger dataset (start_history > 1997)
        try:
            spatial[j, 0] = spatial_j[0]
        except IndexError:
            spatial[j, 0] = spatial_j
        
    cif_trig_anis = np.sum(kwargs['bg_scale'] *
                           10**(kwargs['alpha']*(v_m_anis-kwargs['mc_learning'])) *
                           kwargs['k'] *
                           calc.time_kr(kwargs['time_kr'], kwargs['p'],
                                        kwargs['c'], kwargs['chi'], 
                                        v_m_anis, v_dt[anis_mask, np.newaxis]) *
                           spatial) 

    return cif_trig_anis + cif_trig_iso
                 


def ll_ETAS(parameters, *args):
    '''
    OBJECTIVE FUNCTION (ll_ETAS)
    This function calculates the log-likelihood (ll) of a point-process model
    defined by its conditional intensity function (cif).

    The LL is calculated for earthquakes in 'target_catalog' (zmap format)
    between times 'start_time' and 'end_time'. The cif is calculated based on
    all prior earthquakes, including those in 'learning_catalog' and those
    in 'target_catalog' that occurred prior to the target in question.

    The function takes parameters in the form of a numpy ndarray of form:
        [mu, k, alpha, p, c*, chi**, d*, q*, gamma**]

    * - parameters vary with kernel options
    ** - parameters only present in specific kernels

    Time krs:
        original      - [mu, k, alpha, p, c,   ...
        Kagan         - [mu, k, alpha, p, c_r, ...
        Kagan_general - [mu, k, alpha, p, c_0, chi ...

    Spatial Krs:
        gs            - ... sigma             ]
        gs_sc_Ogata1  - ... d                 ]
        gs_sc_WHJK    - ... f_d               ] 
        gs_sc_Kagan   - ... epsilon, s_r      ]
        pl            - ... d,       q        ]
        pl_sc_Ogata   - ... d,       q        ]
        pl_sc_Zhuang  - ... d,       q,  gamma]
        pl_sc_WHJK    - ... f_d               ]

    *args is an expected tuple of the form:
        (
        learning_catalog,
        target_catalog,
        time_kr,
        spat_kr,
        anis_kr,
        method,
        test_boundary_dist,
        start_time,
        end_time,
        mc_learning,
        mc_target,
        number_of_targets
        b,
        fval_bg,
        anis_param,
        log_param, # bool
        verbose        
        )
    '''
    # First unpacking of parameters & arguments

    # Arguments - passed as an expected tuple
    # *args and tuple form used as neseccary format for optimisation routine
    (learning_catalog, target_catalog,
     time_kr, spat_kr, anis_kr,
     method, test_boundary_dist,
     start_time, end_time,
     mc_learning, mc_target, number_of_targets, b, fval_bg,
     anis_param,
     log_param,
     verbose) = args
    
    if verbose == 2:
        print('***')
        print('parameters : ', parameters)
    
    # exponentiate if paramaters in log format
    if log_param == 1:
        parameters = np.exp(parameters)
            
    # Parameters (varying through optimisation routine):
    # Consistent parameters
    mu = parameters[0]
    k = parameters[1]
    alpha = parameters[2]
    p = parameters[3]
    # Differences in omori-law parameterisation
    if time_kr == 'original':
        c = parameters[4]
        time_param = (p, c, None)
    elif time_kr == 'Kagan':
        c_r = parameters[4]
        time_param = (p, c_r, None)
    elif time_kr == 'Kagan_general':
        c_0 = parameters[4]
        chi = parameters[5]
        time_param = (p, c_0, chi)

    else:
        raise ValueError('Error: must specify valid kernel choice')
    # Differences in spatial kernels
    if spat_kr == 'gs':
        sigma = parameters[-1]
        spat_param = {'sigma': sigma}
    elif spat_kr == 'gs_sc_Ogata1':
        d = parameters[-1]
        alpha_ogata = np.log(10.0) * alpha
        spat_param = {'d': d, 'alpha_ogata': alpha_ogata}
    elif spat_kr == 'gs_sc_WHJK':
        f_d = parameters[-1]
        spat_param = {'f_d': f_d}
    elif spat_kr == 'gs_sc_Kagan':
        epsilon = parameters[-2]
        s_r = parameters[-1]
        spat_param = {'epsilon': epsilon, 's_r': s_r}
    elif spat_kr == 'pl':
        d = parameters[-2]
        q = parameters[-1]
        spat_param = {'d': d, 'q': q}
    elif spat_kr == 'pl_sc_Ogata':
        d = parameters[-2]
        q = parameters[-1]
        alpha_ogata = np.log(10.0) * alpha
        spat_param = {'d': d, 'q': q, 'alpha_ogata': alpha_ogata}
    elif spat_kr == 'pl_sc_Zhuang':
        d = parameters[-3]
        q = parameters[-2]
        gamma = parameters[-1]
        spat_param = {'d': d, 'q': q, 'gamma': gamma}
    elif spat_kr == 'pl_sc_WHJK':
        f_d = parameters[-1]
        spat_param = {'f_d': f_d}
    else:
        raise ValueError('Error: must specify valid kernel choice')

    #reused calcs
    scale = 10.0**(b* (mc_learning - mc_target))
    bg_scale = scale * mu
    cif_bg = bg_scale * fval_bg 

    cif_kwargs = {'time_kr': time_kr, 'spat_kr': spat_kr, 
                  'learning_catalog': learning_catalog, 
                  'target_catalog': target_catalog,
                  'mc_learning': mc_learning, 'bg_scale': scale,
                  'k': k, 'alpha': alpha, 'p': time_param[0], 
                  'c': time_param[1], 'chi': time_param[2],
                  'spat_param': spat_param}    
    
    
    
    # Calculate first term of log-likelihood - 'll_1'
    # LL1 = sum_j=1^(Nt) log(cif(j)) where (j| S<t_j<T) ?
    # freeze cif_kwargs to parloop func by mapping to partial func    
    if anis_kr == 'EAS':
        cif_kwargs = dict(cif_kwargs, **anis_param)
        cif_etas = partial(cif_parloop_eas, **cif_kwargs)
    else:
        cif_etas = partial(cif_parloop, **cif_kwargs)
            
    cif_indices = xrange(0, number_of_targets)
    # using scoop.futures to map in parrallel across several nodes
    cif_trig = list(futures.map(cif_etas, cif_indices))
    cif_trig = np.array(cif_trig).reshape(len(cif_trig), 1)    
    cif = cif_bg + cif_trig    
    ll_1 = np.sum(np.log(cif))
    
 
    # Calc ll_2 (second term) (ll_2_bg + ll_2_trig)

    # ll_2_bg is expected number of bg events, assuming magnitude and
    # spatial integration = 1
    ll_2_bg = bg_scale * (end_time-start_time)

    # ll_2_trig
    v_mag = learning_catalog[:, 5:6]
    v_time = learning_catalog[:, 15:16]
    v_lat = learning_catalog[:, 1:2]
    v_lon = learning_catalog[:, 0:1]
    # choice of calculation method for ll_2_trig
    if method == 'analyt':
        ll_2_trig_i = (scale *
                       k * 10.0**(alpha*(v_mag - mc_learning)) *
                       analyt.integrate_time(time_kr, start_time, end_time,
                                             v_time, v_mag, time_param) *
                       analyt.integrate_space(v_lat, v_lon))
    elif method == 'semi-num--time':
        # Calculate integral of temporal kernel (Omori-Utsu law) numerically
        time_integral = semi_num.integrate_time(time_kr, start_time, end_time,
                                                v_time, v_mag, time_param)
        print('sum num =  ', np.sum(time_integral))
        ll_2_trig_i = (scale *
                       k * 10.0**(alpha*(v_mag - mc_learning)) *
                       time_integral *
                       analyt.integrate_space(v_lat, v_lon))

    elif method == 'semi-num--space':
        # Calculate integral of spatial kernel numerically
        spat_integral = semi_num.integrate_space(spat_kr, v_lat, v_lon, v_mag, 
                                                 spat_param, mc_learning, 
                                                 test_boundary_dist, verbose)
        ll_2_trig_i = (scale *
                       k * 10.0**(alpha*(v_mag - mc_learning)) *
                       analyt.integrate_time(time_kr, start_time, end_time,
                                             v_time, v_mag, time_param) *
                       spat_integral)
    
    elif method == 'semi-num--all':
        # Numerically calculate integral of both kernels independently 
        time_integral = semi_num.integrate_time(time_kr, start_time, end_time,
                                                v_time, v_mag, time_param)
        spat_integral = semi_num.integrate_space(spat_kr, v_lat, v_lon, v_mag, 
                                                 spat_param, mc_learning, 
                                                 test_boundary_dist, verbose)                                                 
        ll_2_trig_i = (scale *
                       k * 10.0**(alpha*(v_mag - mc_learning)) *
                       time_integral *
                       spat_integral)
    else:
        raise ValueError('Error: must specify valid integration method')

    ll_2_trig = np.sum(ll_2_trig_i)
    ll_2 = ll_2_bg + ll_2_trig
    
    f_ll = (ll_1 - ll_2)
    

    if verbose == 2:    
        #print terms for error checking
        print('First term from cif  = ', ll_1)
        print('Second term integral = ', ll_2)
        print('Function value       = ', f_ll) 

    # Return negative of log-likelihood so that minimisation routine maximises
    return -f_ll


def optimise_ETAS(spat_kr, time_kr, anis_kr,
                  learning_catalog, target_catalog,
                  settings):
    '''
    This function sets the various parameter estimates depending on kernel 
    options, including the initial estimates and upper/lower bounds. It then
    calls the optimisation routine and afterwards prints results/output

    The ll of the bg model is calculated first, including a call to cif_bg    
    The parameters and an argument list is then passed to the minimisation
    algorithm to perform the nonlinear constrained multivariate minimisation
    on the objective function, ll_etas, which in turn calculates the 
    log-likelihood (ll) of the etas like model
    
    
    opt_results is an instance of class 
    scipy.optimize.OptimizeResults which is returned from the 
    minimize function. It has attributes as follows:
        opt_results.x(ndarray) - the parameters which provide a solution 
                                 to the optimisation                                
        opt_result.fun(ndarray)- the value of the objective function                             
        success	(bool) - Sucessful exit?
        status	(int) Termination status of the optimiser
        message	(str) Description of the cause of the termination
        nit	(int) Number of iterations performed by the optimiser
        maxcv	(float) The maximum constraint violation
        jac/hess/hess_inv - Values of hessian/jacobian/inverse
        nfev, njev, nhev  (int) Number of function evaluations
    '''
    
    # unpack cars in 'settings' for clarity
    start_learning = settings['start_learning']
    start_time = settings['start_time']
    end_time = settings['end_time']
    b = settings['b']
    random_init_params = settings['random_init_params']
    spatial_dens_bg = settings['spatial_dens_bg']
    mc_learning = settings['mc_learning']
    mc_target = settings['mc_target']
    number_of_targets = settings['number_of_targets']
    method = settings['method']
    test_boundary_dist = settings['test_boundary_dist']
    
   # Calculate LL (log likelihood) of the time-independent bg model

    # Used several times in calcs of cif_bg here and in cif_etas functions
    fval_bg = calc.cif_bg(target_catalog[:, 1],
                          target_catalog[:, 0],
                          spatial_dens_bg)

    # Number of m>=Mc per day, expected rate
    mu_bg = end_time - date2num(datetime(start_learning, 1, 1))
    mu_bg = len(learning_catalog) / mu_bg
    # Conditional intensity function
    cif_bg = 10 ** (b*(mc_learning - mc_target)) * mu_bg * fval_bg
    # Sum over targets of log of conditional intensity function
    ll1_bg = np.sum(np.log(cif_bg))
    # second term: integral over constant rate in time (assuming background
    # is normalized pdf, such that integral = 1)
    ll2_bg = 10**(b*(mc_learning-mc_target)) * (mu_bg*(end_time-start_time))
    ll_bg = ll1_bg - ll2_bg

    # alt calculation
    # number of m>=Mc per day, expected rate
    mu_bg_2 = number_of_targets / (end_time - start_time)
    # conditional intensity function
    cif_bg_2 = mu_bg_2 * fval_bg
    # sum over targets of log of conditional intensity function
    ll1_bg_2 = np.sum(np.log(cif_bg_2))
    # integral over constant rate in time (assuming background is
    # normalized pdf, such that integral =1)
    ll2_bg_2 = mu_bg_2 * (end_time - start_time)
    ll_bg_2 = ll1_bg_2 - ll2_bg_2 


    # repack into obj function ll_ETAS arguments    
    args = (learning_catalog, target_catalog,
            time_kr, spat_kr, anis_kr,
            method, test_boundary_dist,
            start_time, end_time,
            mc_learning, mc_target, number_of_targets, b, fval_bg,
            settings['anis_param'],
            settings['log_param'],
            settings['verbose'])
    
    # See docs of ll_ETAS for parameter format and definitions.
     
    if settings['opt_switch'] == 'off':
        if settings['log_param'] == True:
            print('WARNING: function set to exponentiate params')
        try:
            param = np.load(settings['param_file'])
        except (IOError, AttributeError):
            raise IOError('Parameter file not found: \n\
                           check config.input_settings[\'param_file\'] \n\
                           check /INPUT for correct file in .npy format')
        finally:
            print('Using Parameter Set:  ', param)
        
        ll_f = ll_ETAS(param, *args)       
    
    else:
        mu = end_time - date2num(datetime(start_learning, 1, 1))
        mu = len(learning_catalog) / mu
        lb, ub, param_0 = config.get_param(time_kr, spat_kr, b=b, mu_ub=mu, 
                                           log_param=settings['log_param'])
        # Randomly generate initial estimate of parameters
        if random_init_params == 1:
            param_0_est = param_0            
            rand_param = lambda i: np.random.uniform(lb[i], ub[i])
            param_0 = np.array(map(rand_param, xrange(len(lb))))
        elif random_init_params == 2:
            param_0_est = param_0
            rand_noise = lambda i: np.random.normal(lb[i], ub[i])           
            param_0 += np.array(map(rand_noise, xrange(len(lb))))

        if random_init_params != 0:
            print('Estimated parameters: ', param_0_est)
            print('Random parameters: ', param_0)
            print('% difference: ', (param_0 - param_0_est)/param_0_est * 100)
        # Sanity check: are initial parameter estimates still within bounds?    
        if ((param_0 >= lb) & (param_0 <= ub)).all():
            pass
        else:
            raise ValueError('Initial parameter estimates outside of bounds!')
        if settings['opt_switch'] == 'test':
            ll_f = ll_ETAS(param_0, *args)
        else:
            bounds = tuple((lb[i], ub[i]) for i in xrange(0, len(param_0)))
            options = config.optimisation_options
            op_method = settings['op_code']
            opt_results = scipy.optimize.minimize(ll_ETAS, param_0, 
                                                  bounds=bounds, 
                                                  args=args,
                                                  options=options,
                                                  method=op_method)
            ll_f = opt_results.fun
    
    # Correct negative ll_f returned from ll_ETAS
    ll_f = -ll_f
    
    # Print results of the optimisation to the screen
    print('\n', '='*40, '\n')
    print('Optimisation Finished ... ...')
    print('ll_f  =  ', ll_f)
    print('ll_bg =  ', ll_bg, '\n')
    # Probability gain and information gain per earthquake  
    g = np.exp((ll_f - ll_bg)/number_of_targets)
    i = (ll_f - ll_bg) / number_of_targets
    print('Final Results for: \n\
          \n\
          %s : %s : %s \n\
          \n\
          Expected N_TI     =  %s \n\
          Expected N_TI_2   =  %s \n\
          Observed Nt       =  %s \n\
          \n\
          Probability Gain  =  %s \n\
          Information Gain  =  %s \n' 
          %(spat_kr, time_kr, anis_kr,
            ll2_bg, ll2_bg_2, number_of_targets,
            g, i))
            
            
    try:
        if settings['log_param'] == True:
            opt_results.x = np.exp(opt_results.x)
        param_out = opt_results.x
        f_param_out = settings['results_folder'] + '/param_' +\
                      time_kr + '_' + spat_kr + '_' + anis_kr + '.npy'
        np.save(f_param_out, param_out)
        print('='*40)
        print('Optimisation routine information: ')
        print(opt_results)
        print('='*40)
        aic = calc.aic(i, len(param_out))
        aic_c = calc.aic_c(aic, len(param_out), number_of_targets)
        bic = calc.bic(i, len(param_out), number_of_targets)
        print('Using information gain: ')
        print('AIC  =  ', aic)
        print('AICc =  ', aic_c)
        print('BIC  =  ', bic, '\n')
        print('Using ll: ')
        aic = calc.aic(ll_f, len(param_out))
        aic_c = calc.aic_c(aic, len(param_out), number_of_targets)
        bic = calc.bic(ll_f, len(param_out), number_of_targets)
        print('AIC  =  ', aic)
        print('AICc =  ', aic_c)
        print('BIC  =  ', bic)

    except:
        return
    else:
        pass
     
    try:
        print('='*40)
        print('Inverse Hessian Matrix: \n')
        inverse_hessian = opt_results.hess_inv.todense()
        print(inverse_hessian, '\n')
        param_u = np.sqrt(np.diagonal(inverse_hessian))
        f_param_u_out = settings['results_folder'] + '/param_uncertainties_' +\
                        time_kr + '_' + spat_kr + '_' + anis_kr + '.npy'
        np.save(f_param_u_out, param_u)
        
    except AttributeError:
        print('could not print inverse hessian \n')

    t.split()
    runtime = t.lap_time

    if settings['save_output_data']:
        output_data = [runtime, spat_kr, time_kr, anis_kr, ll_f, g, 
                       i, ll2_bg, number_of_targets, aic, aic_c, bic]                
        with open(settings['output_file'], 'a') as data_file:
            writer = csv.writer(data_file, delimiter=',')
            writer.writerow(output_data)    
        print('\nSaving optimised parameters ... ')
        print('Saving parameter uncertainties from inverse hessian ...')
        print('Saving output data ...')
        print('Results saved to ./' + settings['results_folder'], '\n')

def main():
    '''
    Wrapper function. Performs set-up/clean-up of parameters defined in
    config.py and executes optimise_ETAS for each kernel combination.

    1. Set up - imports earthquake catalogues and cuts to collection & testing
       regions defined by parameters in config.py.
       Calls calc.testing_region_polygon & config
    2. Calls main.optimise_ETAS for each kernel combination.
       This currently runs IN SERIAL
    3. Clean up, write run configurations to file etc.    
    '''
    kernel_options = config.kernel_options
    run_options = config.run_options
    main_param = config.main_param
    input_settings = config.input_settings
    anis_param = config.anis_param
    # Generate Date/Time stamp for output file
    if run_options['results_file'] == 'auto':
        run_options['results_file'] = (strftime('%d-%m-%y_%H%M%S'))
    if run_options['results_folder'] == 'auto':
        run_options['results_folder'] = (strftime('%d-%m-%y_%H%M%S'))
    # Generate output file (comma seperated value file, labels set here)        
    if run_options['save_output_data']:
        labels = ['Runtime', 'Spatial Kernel', 'Temporal Kernel',
                  'Anisotropic Kernel', 'Optimised Function',
                  'Probability Gain', 'Information Gain', 'Expected N_TI',
                  'Observed N_TI', 'AIC', 'AICc', 'BIC']
        run_options['output_file'] = run_options['results_folder'] + '/' + \
                                     run_options['results_file'] + '.csv'
        
        if not os.path.exists(os.path.dirname(run_options['output_file'])):
            try:
                os.makedirs(os.path.dirname(run_options['output_file']))
            except OSError as exc: # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise        
        with open(run_options['output_file'], 'w') as data_file:
            writer = csv.writer(data_file, delimiter=',')
            writer.writerow(labels)    
    # Store as nested dicts for easier passing of variable clusters
    # Convert [y m d h m s ms] to single number(note: ordinal is 1,1)
    # meaning date2num() gives matlab datenum() - 366
    main_param['start_time'] = date2num(datetime(*main_param['start_time']))
    main_param['end_time'] = date2num(datetime(*main_param['end_time']))
    settings = dict(main_param, **input_settings)
    settings = dict(settings, **run_options)
    settings['anis_param'] = anis_param

    ##########################################################################

    # Load input catalog and cut events below threshold magnitude(mc_learning)
    # Format for the input catalog
    # in zmap format: lon lat decimalyr mo dy mag depth hr min sec
    input_catalog = np.load(settings['input_catalog'])
    # Cutting once now greatly improves speed
    input_catalog = input_catalog[input_catalog[:, 5]
                                  >= main_param['mc_learning'], :]

    # Cut catalogue to collection region
    # First define collection region:
    # Load collection_area data, define extent of spatial bins and
    # define boundary polygon with testing_region_polygon()
    #f = scipy.io.loadmat(settings['collection_area'])
    #collection_nodes = f['mArea']
    collection_nodes = np.load(settings['collection_area'])    
    # Upper and lower lat/lon ends of spatial bins
    coll_min_lon = collection_nodes[:,0:1] - 0.05
    coll_max_lon = collection_nodes[:,0:1] + 0.05
    coll_min_lat = collection_nodes[:,1:2] - 0.05
    coll_max_lat = collection_nodes[:,1:2] + 0.05 
    # Bins of interest for forecast in format: 'lonmin lonmax latmin latmax'
    coll_bins = np.hstack((coll_min_lon, coll_max_lon,
                           coll_min_lat, coll_max_lat))                             
    coll_polygon = calc.testing_region_polygon(coll_bins)
    # MAKE SURE IN SAME LAT, LON FORMAT AS POLYGON
    cat_points = np.column_stack((input_catalog[:, 1], input_catalog[:, 0]))
    coll_path = Path(coll_polygon)
    in_coll_poly = coll_path.contains_points(cat_points)
    coll_catalog = input_catalog[in_coll_poly == 1, :]

    cat = lambda i: np.concatenate((coll_catalog[i, 2:5],
                                    coll_catalog[i, 7:10]))
    time_array = np.array([date2num(datetime(*map(int, cat(i))))
                           for i in xrange(0, len(coll_catalog))])

    # append the time_array to coll_catalog in col 16
    coll_catalog = np.hstack((coll_catalog,
                              time_array.reshape(len(time_array), 1)))
    learning_catalog = coll_catalog[(coll_catalog[:, 2]
                                     >= settings['start_learning']) &
                                    (coll_catalog[:, 15]
                                     < settings['end_time']) &
                                    (coll_catalog[:, 5]
                                     >= settings['mc_learning']), :]
    # Define testing region
    # Bins of interest for forecast in format: 'lonmin lonmax latmin latmax'                           
    test_bins = np.load(input_settings['spatial_test_bins'])
    test_polygon = calc.testing_region_polygon(test_bins)
    cat_points = np.column_stack((coll_catalog[:, 1], coll_catalog[:, 0]))
    test_path = Path(test_polygon)
    in_test_poly = test_path.contains_points(cat_points)
    target_catalog = coll_catalog[in_test_poly == 1, :]

    # Cut catalog above mc_target, between start_time and end_time
    target_catalog = target_catalog[(target_catalog[:, 15]
                                     >= settings['start_time']) &
                                    (target_catalog[:, 15]
                                     < settings['end_time']) &
                                    (target_catalog[:, 5]
                                     >= settings['mc_target']),
                                    :]
    additional_info = {'number_of_targets': len(target_catalog),
                       'number_of_learning_events': len(learning_catalog)}
    settings = dict(settings, **additional_info)


    # Main Calculation and function calls
    spat_kr = kernel_options['spatial_kernels']
    time_kr = kernel_options['time_kernels']
    anis_kr = kernel_options['anisotropic_kernels']
    n_spat_kr = len(spat_kr)
    n_time_kr = len(time_kr)
    n_anis_kr = len(anis_kr)
    n_runs = n_spat_kr * n_time_kr * n_anis_kr
    # split time so set-up/file loads do not count towards runtime    
    t.split(message='Split - Set-up Complete')
    for n in xrange(1, n_runs+1):
        spat_index = ((n-1)//n_anis_kr//n_time_kr) % n_spat_kr
        time_index = ((n-1)//n_anis_kr) % n_time_kr
        anis_index = (n-1) % n_anis_kr
        optimise_ETAS(spat_kr[spat_index],
                      time_kr[time_index],
                      anis_kr[anis_index],
                      learning_catalog,
                      target_catalog,
                      settings)
                      

    if settings['record_config']:
        # Output run settings and parameters to text file
        config_filename = run_options['results_folder'] + '/' + \
                          run_options['results_file'] + '_CONFIG.txt'
        if not os.path.exists(os.path.dirname(config_filename)):
            try:
                os.makedirs(os.path.dirname(config_filename))
            except OSError as exc: # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
                          
                          
        with open(config_filename, 'a+') as cfgfile:
            config_out = configparser.ConfigParser()
            config_out.add_section('run_options')
            config_out.add_section('main_param')
            config_out.add_section('input_settings')
            config_out.add_section('anis_param')
            config_out.add_section('additional_info')
            for section in config_out.sections():
                section_dict = eval(section)
                for key in section_dict:
                    config_out.set(section, key, str(section_dict[key]))
            config_out.write(cfgfile)
    else:
        print('Config not recorded! \n')

if __name__ == '__main__':
    t = timing.Timer()
    main()
    t.end()
