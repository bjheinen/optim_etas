# -*- coding: utf-8 -*-
'''
   Calculate the integral terms of the log-likelihood equation 
   for ETAS-like models.
   ---   
   Method used: Numerical Integration
   
   These functions are able to integrate numerically any combination of 
   the following spatial/temporal kernels:
   
      temporal: ['original', 'Kagan', 'Kagan_general']
      spatial:  ['gs', 'gs_sc_Ogata1', 'gs_sc_WHJK', 
                 'gs_sc_Kagan', 'pl', 'pl_sc_Ogata', 
                 'pl_sc_WHJK', 'pl_sc_Zhuang']
   
    However the spatial and temporal kernels are integrated seperately so
    this is not a true numerical integration method.    
              
   This provides a comparison benchmark to test the feasibility/accuracy
   of numerical integration methods which can potentially be used on models
   where analytical methods fail.
'''
from __future__ import print_function
from past.builtins import xrange
from past.builtins import map
#from past.builtins import zip

__author__ = "Benedict J Heinen"
__copyright__ = "Copyright 2015, Benedict J Heinen"
__email__ = "benedict.heinen@gmail.com"

# Built-in modules
from functools import partial
import multiprocessing
#SciPy Stack modules
from matplotlib.path import Path
import numpy as np
#np.set_printoptions(threshold=np.nan)
import scipy.integrate
# Third Party Modules
from numba import autojit
# This package/author modules
from distance import v_distance as distance


@autojit
def omori_func(t, t_i, p, c_i):
    # % Omori law: phi(t-t_i) = (p-1) c^(p-1) / (T-t_i+c)^p 
    return (c_i**(p - 1) * (p - 1) / (t - t_i + c_i)**(p))


def return_time_integral(i, **kwargs):
    res, err = scipy.integrate.quad(omori_func, 
                           max(kwargs['start_time'], kwargs['v_time'][i, 0]), 
                           kwargs['end_time'],
                               args = (kwargs['v_time'][i, 0], kwargs['p'], 
                                       kwargs['c_i'][i, 0]))
    return res


def integrate_time(time_kr, start_time, end_time,
                   v_time, v_mag, time_param):
    '''
    Calculates the integral of Omori's law over the period from the 
    start_time to end_time given eq start time v_times for eq v_mag.
    
    p, c, chi = time_param

    If the Omori law was kicked off at times eq_times AFTER the start time S,
    then only T-vEqTimes is relevant.
    If the Omori law was started BEFORE S, then we only want to integrate
    that part of the Omori law that contributed between S and T.

    Omori law:
    phi(T-t_i) = (p-1) c^(p-1) / (T-t_i+c)^p    for T>t_i, zero otherwise.
    '''
    p, c, chi = time_param
    if time_kr == 'original':
        c_i = np.tile(c, (len(v_time), 1))
    elif time_kr == 'Kagan':
        c_i = c * 10**((v_mag-4)/2)
    elif time_kr == 'Kagan_general':
        c_i = c * 10**(chi * (v_mag-4))
    else:
        raise ValueError('Invalid Spatial Kernel?! --- Check: config.py')

    time_kwargs = {'start_time': start_time, 'end_time': end_time,
                   'v_time': v_time, 'p': p, 'c_i': c_i}
    integrate = partial(return_time_integral, **time_kwargs)   
                             
    n_integrals = xrange(0, len(v_time)) 

    # vectorise integration and map in parrallel using available processors    
    pool = multiprocessing.Pool()
    result = pool.map(integrate, n_integrals, chunksize=10)
    pool.close()    
    omori_integrals = np.array(result).reshape(len(result), 1)
    #omori_integrals = map(np.array, zip(*result))[0].reshape(len(result), 1)
    # errs = map(np.array, zip(*result))[1].reshape(len(result), 1)
    return omori_integrals

#def bound_dist(i, **kwargs):
#    return np.min(distance(kwargs['bound_points'][:,1], 
#                           kwargs['bound_points'][:,0],
#                           kwargs['v_lat'][i, 0], kwargs['v_lon'][i, 0], 
#                           units='km'))


# functions to integrate , f(r, theta) in polar coordinates
# gaussian function
@autojit
def gs_func(x, y, sigma_sq_2, pi):
    return ((1/(pi*sigma_sq_2)) * 
            np.exp(-1*(((x*x)+(y*y))/sigma_sq_2)))
# power law function
@autojit
def pl_func(x, y, d_sq, q, pi):   
   return (q-1)/(pi*d_sq) * (1+(((x*x)+(y*y))/d_sq))**(-q)  

def integrate_space(spat_kr, v_lat, v_lon, v_m, 
                    spat_param, mc_learning, test_boundary_dist, verbose):
    '''
    Calculates the integral of the spatial kernel, 'spat_kr', centred at the 
    earthquake locations v_lat/v_lon over the test volume specified by the
    CSEP testing region.
    
    The approach taken is to integrate discretely cell by cell

    v_m is also passed to this function as the bandwidth (scale factor) of the
    kernel may scale with magnitude (model/kernel dependent)    
    
    '''

    # Define functions to integrate, f(r, theta) in polar coordinates
    # Gaussian (gs) and power-law  (pl) functions:    
    number_of_events = len(v_m)
    pi_180 = np.pi/180.0
    if spat_kr == 'gs':
        # standard deviation
        spat_sc = np.tile(spat_param['sigma'],(number_of_events, 1))
        kr_law = 0 
    elif spat_kr == 'gs_sc_Ogata1':
        # variance calculated - hence sqrt
        spat_sc = np.sqrt((spat_param['d'] * 
                           np.exp(spat_param['alpha_ogata'] * 
                           (v_m-mc_learning))))
        kr_law = 0
    elif spat_kr == 'gs_sc_WHJK':
        # standard deviation
        spat_sc = 0.5 + spat_param['f_d'] * 0.01 * 10**(0.5*v_m)        
        kr_law = 0
    elif spat_kr == 'gs_sc_Kagan':
        # variance calculated - hence sqrt
        spat_sc = np.sqrt(((spat_param['epsilon']**2) + 
                           spat_param['s_r'] * 10**(v_m - 4)))
        kr_law = 0
    elif spat_kr == 'pl':
        # d
        spat_sc = np.tile(spat_param['d'], (number_of_events, 1))
        func = pl_func
        kr_law = 1
    elif spat_kr == 'pl_sc_Ogata':
        # d^2  calculated - hence sqrt     
        spat_sc = np.sqrt(spat_param['d'] * 
                          np.exp(spat_param['alpha_ogata']*(v_m-mc_learning)))
        kr_law = 1
    elif spat_kr == 'pl_sc_Zhuang':
        # d^2  calculated - hence sqrt
        spat_sc = np.sqrt((spat_param['d'] * 
                           np.exp(spat_param['gamma']*(v_m - mc_learning))))
        kr_law = 1
    elif spat_kr == 'pl_sc_WHJK':
        # d
        spat_sc = 0.5 + spat_param['f_d'] * 0.01 * 10**(0.5 * v_m)
        spat_param['q'] = 1.5
        kr_law = 1
    else:
        raise ValueError('Error: must specify valid kernel choice')
        
    if kr_law == 0:
        func = gs_func
        exclusion_limit = 2 * 5.92 * spat_sc
    else:
        func = pl_func
        exclusion_limit = 100 * spat_sc
        
    test_bins = np.load('INPUT/SpatialTestBins.npy')
    # format: 'lonmin lonmax latmin latmax'
    number_of_cells = len(test_bins)
    cell_lon = (test_bins[:, 0] + test_bins[:, 1])/2.0
    cell_lat = (test_bins[:, 2] + test_bins[:, 3])/2.0

    integral_eq = np.zeros((number_of_events, 1))  
    dont_calculate = np.zeros(len(v_lat))
    if test_boundary_dist:
        bound_points = np.array([[-125.2, 43.0], [-119.0, 43.0], 
                                 [-119.0, 39.4], [-114.0, 35.7], 
                                 [-113.1, 34.3], [-113.5, 32.9], 
                                 [-113.6, 32.2], [-114.5, 31.7], 
                                 [-117.1, 31.5], [-117.9, 31.9], 
                                 [-118.4, 32.8], [-121.0, 33.7], 
                                 [-121.6, 34.2], [-123.8, 37.7], 
                                 [-125.4, 40.2], [-125.4, 40.5], 
                                 [-125.2, 43.0]])                     
        boundary = Path(bound_points)
        eq_points = np.column_stack((v_lon, v_lat))
        # returns a bool array
        inside = boundary.contains_points(eq_points)    
        
        #bd_kwargs = {'bound_points': bound_points, 
        #             'v_lat': v_lat, 'v_lon': v_lon}
        #dist = partial(bound_dist, **bd_kwargs)
        dist = lambda i: np.min(distance(bound_points[:,1], bound_points[:,0],
                                         v_lat[i, 0], v_lon[i, 0],units="km"))
        min_b_dist = map(dist, xrange(len(v_lat)))
        #pool = multiprocessing.Pool()
        #min_b_dist = pool.map(dist, xrange(len(v_lat)), chunksize=250)
        #pool.close()        
        min_b_dist = np.array(min_b_dist).reshape(len(min_b_dist), 1)
        dont_calculate = exclusion_limit < min_b_dist 
    
    for i in xrange(0, number_of_events):  
        integral_i = np.zeros(number_of_cells)

        if verbose == 1:       
            try:    
                if not dont_calculate[i] and not inside[i]:
                    print('Earthquake (i) = ', i, ': \n\
                          occured outside test region but is bleeding in')  
            except NameError:
                pass
        
        if dont_calculate[i] and test_boundary_dist:
            # 1 if inside, 0 if outside
            integral_i = float(inside[i]/1)
        
        else:
            scale = spat_sc[i, 0]
            if kr_law == 0:
                sigma_sq_2 = scale * scale * 2.0    
                fargs = (sigma_sq_2, np.pi)
            else:
                d_sq = scale * scale
                fargs = (d_sq, spat_param['q'], np.pi)            
            distance_i = distance(np.tile(v_lat[i, 0], (number_of_cells)),
                                  np.tile(v_lon[i, 0], (number_of_cells)), 
                                  cell_lat, cell_lon, units='km')
            ind = (distance_i < exclusion_limit[i, 0]).nonzero()
            int_cells_indices = ind[0].reshape(len(ind[0]), 1)
            number_of_int_cells = len(int_cells_indices[:,0])
            if number_of_int_cells > 0:

                r_min,az_min = distance(np.tile(v_lat[i,0],(number_of_cells,1)),
                                        np.tile(v_lon[i,0],(number_of_cells,1)), 
                                        test_bins[:, 2:3], test_bins[:, 0:1], 
                                        units='km', nargout=2)
                r_max,az_max = distance(np.tile(v_lat[i,0],(number_of_cells,1)),
                                        np.tile(v_lon[i,0],(number_of_cells,1)), 
                                        test_bins[:, 3:4], test_bins[:, 1:2], 
                                        units='km', nargout=2)
                       
                x_min = r_min * np.sin(az_min * pi_180)
                y_min = r_min * np.cos(az_min * pi_180)
                x_max = r_max * np.sin(az_max * pi_180)
                y_max = r_max * np.cos(az_max * pi_180)
                
            integral_res = np.zeros(number_of_int_cells)            
            for j in xrange(number_of_int_cells):
                k = int_cells_indices[j, 0]  
                x_0 = x_min[k, 0]
                x_1 = x_max[k, 0]
                y_0 = y_min[k, 0]
                y_1 = y_max[k, 0]
                res, err = scipy.integrate.dblquad(func, 
                                                   x_0, x_1, 
                                                   lambda x: y_0, 
                                                   lambda x: y_1, 
                                                   args=fargs)                
                integral_res[j] = res
            integral_i[int_cells_indices] = integral_res 
        integral_eq[i, 0] = np.sum(integral_i)
        if verbose == 1:
            if test_boundary_dist:
                if 0.01 < integral_eq[i, 0] < 0.99:
                    print('eq: i = ', i, '\ninside = ', inside[i], '\n\
                          contributed I = ', integral_eq[i],
                          'to the spatial integral')

    #print integral_eq for comparison with analytic
    #print(np.sum(integral_eq))
    return integral_eq
    