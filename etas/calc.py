# -*- coding: utf-8 -*-
'''
   Provides functions for calculations required in 

   Functions:
   
       time_kr
       
       spat_kr
       distance
       
       cif_bg
       
       testing_region_polygon
                     
'''
from past.builtins import xrange

__author__ = "Benedict J Heinen"
__copyright__ = "Copyright 2015, Benedict J Heinen"
__email__ = "benedict.heinen@gmail.com"
import numpy as np
from distance import v_distance as distance

def rupture_length(mag):
    '''calculate rupture length of an eqk of magnitude mag'''
    return 0.01 * 10**(0.5 * mag)


def time_kr(time_kr, p, c, chi, v_m, v_dt):
    '''
    Evaluates the Omori Law

    c parameter used in calculation of time_kr varies with kernel used:

    'original':
        Evaluates the Omori-Law pdf at values u for given parameters c, p:
        f(u) = (1-p) c^(1-p) /(u+c)^p
        where u = t_j - t_i >=0.
        requires p>1 for normalization.

    'Kagan':
        Evaluates the Omori law using Kagan's parameterization of
        the c-value. c = c_r * 10^([m+6]/2)

    'Kagan_general':
        Evaluates the Omori law using a generalized Kagan's parameterization
        of the c-value. c = c_0 * 10^(chi*m)
    '''
    if time_kr == 'Kagan':
        c *= 10**((v_m-4)/2.0)
    elif time_kr == 'Kagan_general':
        c *= 10**(chi*(v_m-4))
    else:
        pass
    return (p-1.0) * c**(p-1.0) / (v_dt+c)**p


def spat_kr(spat_kr, v_lat, v_lon, 
            v_loc_lat, v_loc_lon, 
            v_m, mc_learning, spat_param):
    '''
    Evaluates the spatial triggering kernel, here assumed to be a
    two-dimensional, isotropic normal density with standard deviation sigma

    Input:
    spat_kr   kernel used
    vLat      vector of latitudes of target locations to evaluate normal pdf
    vLon      vector of longitudes of target locations to evaluate normal pdf
    vLocLat   vector of lats of locations of normal densities (past quakes)
    vLocLon   vector of lons of locations of normal densities (past eq)

    mc_learning and spat_param

    OTHER PARAMS
    '''
    # calculate distances in km
    r = distance(v_lat, v_lon, v_loc_lat, v_loc_lon, units='km')
    # f(r,theta) in polar coordinates
    # such that int_0^infty int_0^2pi f(r,theta) * r * dr * dtheta = 1
    if spat_kr == 'gs':
        sigma_sq2 = 2 * spat_param['sigma'] * spat_param['sigma']
        eval_spatial = (1.0/(np.pi*sigma_sq2)) * np.exp(-1.0 * ((r*r)/sigma_sq2) )
    elif spat_kr == 'gs_sc_Ogata1':
        alpha_ogata = spat_param['alpha_ogata']
        d = spat_param['d']
        sigma_sq2 = 2 * d * np.exp(alpha_ogata * (v_m - mc_learning))
        eval_spatial = (1.0/(np.pi*sigma_sq2)) * np.exp( -1.0 * ((r*r)/sigma_sq2) )
    elif spat_kr == 'gs_sc_WHJK':
        # eq 18 in Werner et al. 2011
        f_d = spat_param['f_d']
        sigma = 0.5 + f_d * 0.01 * 10**(0.5 * v_m)
        sigma_sq2 = 2 * sigma * sigma
        eval_spatial = (1.0/(np.pi*sigma_sq2)) * np.exp( -1.0 * ((r*r)/sigma_sq2) )
    elif spat_kr == 'gs_sc_Kagan':
        # eq 26 in Kagan & Jackson 2012
        epsilon = spat_param['epsilon']
        s_r = spat_param['s_r']
        sigma = np.sqrt((epsilon*epsilon) + (s_r*s_r) * 10**(v_m-4.0))
        sigma_sq2 = 2 * sigma * sigma
        eval_spatial = (1.0/(np.pi*sigma_sq2)) * np.exp(-1.0 * ( (r*r)/sigma_sq2) )

    elif spat_kr == 'pl':
        d = spat_param['d']
        q = spat_param['q']
        eval_spatial = ((q-1.0)/(np.pi*d*d)) * ( 1.0 + ((r*r)/(d*d)) )**(-q)
    elif spat_kr == 'pl_sc_Ogata':
        d = spat_param['d']
        q = spat_param['q']
        alpha_ogata = spat_param['alpha_ogata']
        exp_term = np.exp(alpha_ogata * (v_m - mc_learning))        
        eval_spatial = (1.0 + ((r*r)/(d*exp_term)))**(-q)
        eval_spatial *= ((q-1.0)/(np.pi*d*exp_term))
    elif spat_kr == 'pl_sc_Zhuang':
        d = spat_param['d']
        q = spat_param['q']
        gamma = spat_param['gamma']
        exp_term = np.exp(gamma * (v_m - mc_learning))             
        eval_spatial = (q-1.0) / (np.pi*d*exp_term)
        eval_spatial *= ( 1.0 + ((r*r)/(d*exp_term)) )**(-q)
    elif spat_kr == 'pl_sc_WHJK':
        f_d = spat_param['f_d']
        d = 0.5 + f_d * 0.01 * 10**(0.5 * v_m)
        eval_spatial = ((1.5-1.0)/(np.pi*d*d))  * (1.0+((r*r)/(d*d)))**(-1.5)
    else:
        raise ValueError('Must specify valid spatial kernel')

    return eval_spatial

def cif_bg(v_lat, v_lon, bg_input_file):
    '''
    This function returns the likelihood of the locations vX, vY [lat, lon],
    as epicenters from a background model (read from bg_input_file).

    Returns the variable cif
    '''

    # First load the spatial density data. The array is stored in .npy form
    # The format is: lonmin lonmax latmin latmax density (in [km]^(-2))
    mu = np.load(bg_input_file)

    cif = np.zeros((len(v_lat), 1))

    n = 100000.0
    for i in xrange(0, len(v_lat)):
        cif[i] = mu[(np.round(n*mu[:, 2]) <= np.round(n*v_lat[i])) &
                    (np.round(n*mu[:, 3]) > np.round(n*v_lat[i])) &
                    (np.round(n*mu[:, 0]) <= np.round(n*v_lon[i])) &
                    (np.round(n*mu[:, 1]) > np.round(n*v_lon[i])), 4]
    return cif


def testing_region_polygon(region_bins):
    '''
       For the specified forecast, generate a GMT-ready outline of the testing
       region covered by the forecast.  To do this, we find the min/max
       latitude, and at each latitude point in this range, find the min/max
       longitude point.  We then collect these lat/lon points in a format that
       is ready for GMT plotting.

       Parameters:

          region_bins - argument given must be an nx4 numpy array in the format:
                        lonmin lonmax latmin latmax

       Output:
          polygon_points - numpy array of points representing border of the
                           region covered by 'region_bins'

       Original Matlab Code - Copyright (C) 2008 by J. Douglas Zechar
    '''
    n_min_lat = np.unique(region_bins[:, 2])
    n_max_lat = np.unique(region_bins[:, 3])

    n_polygon_points = len(n_min_lat) * 4.0
    polygon_points = np.zeros((int(n_polygon_points + 1), 2))

    for i in xrange(1, len(n_min_lat)+1):
        region_index = (region_bins[:, 2] == n_min_lat[i-1])
        min_lon_current_lat = min(region_bins[region_index, 0])

        polygon_points[i * 2 - 2, 0] = n_min_lat[i-1]
        polygon_points[i * 2 - 2, 1] = min_lon_current_lat
        polygon_points[i * 2 - 1, 0] = n_max_lat[i-1]
        polygon_points[i * 2 - 1, 1] = min_lon_current_lat

    n_min_lat[::-1].sort()
    n_max_lat[::-1].sort()

    for j in xrange(1, len(n_min_lat)+1):
        region_index = (region_bins[:, 3] == n_max_lat[j-1])
        max_lon_current_lat = max(region_bins[region_index, 1])

        polygon_points[i*2 + j*2 - 2, 0] = n_max_lat[j-1]
        polygon_points[i*2 + j*2 - 2, 1] = max_lon_current_lat
        polygon_points[i*2 + j*2 - 1, 0] = n_min_lat[j-1]
        polygon_points[i*2 + j*2 - 1, 1] = max_lon_current_lat

    polygon_points[i * 2 + j * 2, 0] = polygon_points[0, 0]
    polygon_points[i * 2 + j * 2, 1] = polygon_points[0, 1]

    return polygon_points
    
def aic(ll, k):
    '''
    Returns the Akaike information criterion, AIC. 
    This is a measure of the relative quality of statistical models for a 
    given set of data. It penalises models with more parameters (and hence 
    more uncertainty)
    
    k  - no. of parameters
    ll - maximized log-likelihood    
    '''
    return (2.0 * k) - (2 * ll)

def aic_c(aic, k, n):
    '''
    Corrected AIC (see above)
    '''
    return aic + (((2.0*k*(k + 1)) / (n - k - 1.0))) 
    
    
def bic(ll, k, n):
    '''
    Returns the Bayesian information criterion, BIC.
    A lower BIC is preferred. 
    The principle is that when fitting models it is possible to increase the 
    likelihood by adding parameters, but doing so may result in overfitting,
    so this measure penalises models with more parameters
    
    k  - no. of parameters
    n  - no. of data points used    
    ll - maximized log-likelihood
    '''
    return -(2.0 * ll) + (k * np.log(n))
    