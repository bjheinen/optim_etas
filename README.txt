README
optim_etas v0.10
----------------
optim_etas compares variations of the ETAS class of statistical earthquake 
forecasting models. It performs constrained non-linear minimisation of the 
log-likelihood functions from different variations of the ETAS model, or with
different data. It can be used to estimate paramater sets for models, or to
assess the information gain of different parameter/data sets.


AUTHORS
=======

Benedict J. Heinen <benedict.heinen@gmail.com>
Maximilian Werner  <max.werner@bristol.ac.uk>


LICENCE
=======

optim_etas
Copyright (C) 2016 Benedict J. Heinen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (version 3).

Please see LICENCE.txt for more information


FILE LIST
=========

optim_etas/
|
|---README.txt
|---LICENCE.txt
|
+---etas/
|   |---etas.py
|   |---config.py
|   |---calc.py
|   |---analyt.py
|   |---semi-num.py
|   |---distance.py
|   |---timing.py
|   +---INPUT/
|       |---input_catalog.npy
|       |---CIF_conan_m2M4_km2.npy
|       |---RELMCollectionArea.npy
|       |---SpatialTestBins.npy
|       
+---hpc/
|   +---etas/
|       |---etas_scoop.py
|       |---config.py
|       |---calc.py
|       |---analyt.py
|       |---semi-num.py
|       |---distance.py
|       |---timing.py   
|       
*


DEPENDENCIES
============

Python >=2.7.10 or Python >=3(.5.2)
    NOTE!: Python 3.x has not been extensively tested at this time
    https://www.python.org/ for more information, docs and download
    A Scientific Python distribution will bundle Python with the Scipy stack
    and a package manager. A comparison can be found here: 
    	http://www.scipy.org/install.html 
    or we can reccomend Anaconda Python: 
    	(free for academic use: https://www.continuum.io/)

SciPy >=0.16.0
    http://www.scipy.org/ - Project Website
    https://github.com/scipy/scipy - Library Source
    scipy.optimize is used for optimisation routines
    scipy.integrate is used for numerical integration

NumPy >=1.9.2 (Tested on 1.9.2 & 1.10.1 only)
    http://www.numpy.org/ - Project Website
    https://sourceforge.net/projects/numpy/files/ - Source/Binaries

matplotlib >=1.4.3 (Tested on 1.4.3 & 1.5.0 only)
    http://matplotlib.org/

Numba >=0.21.0 (Tested 0.21.0/0.22.1/0.23.1, likely to work on other versions)
    http://numba.pydata.org/
    Numba requires llvm (http://llvm.org/) which may not be included with 
    your Python distribution (especially if using your own build of python). 
    This can take some time to build; use llvm-lite.

future >= 0.15.2
    https://pypi.python.org/pypi/future
    The module future is used to provide simultaneous Py2.x & Py3.x
    compatability. The specific module used is past.builtins which
    is not included as part of the standard library.
    Use 'pip install future' or 'conda install future' to install
    this module to your environment if an ImportError is shown.

SCOOP =0.7.2 (Tested on this version only)
    https://github.com/soravux/scoop
    http://scoop.readthedocs.io/en/latest/index.html
    SCOOP (Scalable COncurrent Operations in Python) is a module
    for distributed programming which provides a parallel map function
    which works across any number of compute nodes or remote units accessible 
    via passwordless ssh. 
    It is used here in the HPC version of the code ONLY. Under: 
       optim_etas/hpc/etas/
    The scoop version should be run with:
       python -m scoop -vv etas_scoop.py
    The -vv flag is optional, and is used to increase verbosity,
    a -n flag can be used to specify the number of workers to spawn. 

    SCOOP natively supports most cluster scheduling managers, including
    Sun Grid Engine (SGE), Torque (PBS-compatible, Moab, Maui) and SLURM.


PACKAGE DESCRIPTION & USAGE
===========================

Short Description & Results Format
----------------------------------

Run the code at the command line with 'python etas.py'

Options for the run including catalog conditions, input/output files, 
optimisation options and kernel combinations are found in config.py

Set 'results_folder' and 'results_file' within config.py to manually
select paths for output data. Setting this option to 'auto' (default)
will set paths and filenames to a timestamp of the run.

Screen printed output from the run should be redirected manually to save it.
This information is useful for checking general information, convergence status
messages, iterations performed etc.

Optimised parameters are stored as numpy arrays in .npy files.
Complimentary .npy files store the parameter uncertainties.
Filenames reflect runs using the format:
   param_timekr_spatkr_aniskr.npy and
   param_uncertainties_timekr_spatkr_aniskr.npy

General output data is saved as comma seperated values in the same folder
The data saved are:
   - Runtime (in seconds)
   - Spatial, temporal and anisotropic kernels used
   - Value of the optimised log-likelihood function
   - Probability Gain
   - Information Gain
   - Expected and Observed number of events
   - AIC, AICc and BIC

The contents of config.py are also saved by default, this will record run
parameters and options.


Note on parameter uncertainties derived from the Inverse Hessian
---------------------------------------------------------------- 

The parameter vector, theta, is the vector which provides a minimum of the 
objective function (thereby giving a maximum of the log-likelihood).
The algorithm used to estimate theta, l-bfgs-b, uses a quasi-newton approach.
Therefore a local analysis of the curvature of the objective function can be
used to estimate the covariance of the parameters (e.g. Menke, 1989).
The local curvature can be estimated by the inverse Hessian matrix, 
itself already (estimated) computed by the l_bfgf_b algorithm used here.
The square root of the diagonal elements of this covariance matrix define 
the estimated standard deviation of computed parameters.
However it is important to note that this is usually an underestimate of the
uncertainty (therefore tighter confidence bounds should be chosen).


Modules and Functions
---------------------

etas.py (executable)

The main wrapper for the code.
Contains functions to set-up options, run optimisation routines for parameter
estimations, calculate log-likelihood and conditional intensity functions.

    etas.main

    Wrapper function. 
    1. Set up - imports earthquake catalogues and cuts to collection & testing
       regions defined by parameters in config.py.
       Calls calc.testing_region_polygon & config
    2. Calls etas.optimise_ETAS for each kernel combination.
       This currently runs IN SERIAL
    3. Clean up, write run configurations to file etc.
    
    --------------------------------------------------------------------------

    etas.optimise_ETAS

    Retrieves ETAS model parameters or estimates, calculates log-likelihood
    of the background model and either calculates log-likelihood of the 
    parameterised ETAS model or uses a non-linear optimisation routine
    to estimate the paramters based on the maximum likelihood method 
    (minimising the negative log-likelihood).

    Results are cleaned up, formatted and printed to the screen here.
    Parameter estimated are saved in .npy format in RESULTS/

    ll_ETAS is called to calculate the log-likelihood of the ETAS model or is
    used as the objecive function for the optimisation routine.

    Calls config.get_param to retrieve parameter estimates if optimisation
    is set to on. Else parameter estimated are randomly geenrated between the
    upper and lower bounds or parameters are retrieved from INPUT/.
    
    Calls calc.cif_bg to calculate the conditional intensity function of the
    background model.

    Calls calc.aic/bic/aic_c to calculate AIC/BIC/corrected AIC

    Results from the optimisation routine (from scipy.optimise,
    default='l-bfgs-b') are of class scipy.optimize.OptimizeResults
    with the following attributes:

        opt_results.x (ndarray) - the parameters which provide a solution 
                                  to the optimisation                                
        opt_result.fun(ndarray) - the value of the objective function                             
        success	      (bool)    - Sucessful exit?
        status	      (int)     - Termination status of the optimiser
        message	      (str)     - Description of the cause of the termination
        nit	      (int)     - Number of iterations performed
        maxcv	      (float)   - The maximum constraint violation
        hess_inv                - Values of hessian/jacobian/inverse
        nfev          (int)     - Number of function evaluations
    
    --------------------------------------------------------------------------

    etas.ll_ETAS

    This function calculates the log-likelihood of a point-process model
    defined by its conditional intensity function cif.

    The LL is calculated for earthquakes in 'target_catalog' (zmap format)
    between times 'start_time' and 'end_time'. The cif is calculated based on
    all prior earthquakes, including those in 'learning_catalog' and those
    in 'target_catalog' that occurred prior to the target in question.

    1. Parameters are unpacked and set up for the run option and ETAS model
    2. The first term in the log-likelihood equation (ll_1) is calculated,
       where ll_1 = sum_j=1^(Nt) log(cif(j)) where (j| S<t_j<T)
       This calculation is done in parallel using the standard library
       multiprocessing module and mapping cif_parloop or cif_parloop_EAS
       to a partial function using functools.Partial    
       This provides a massive increase in performance, especially noticeable
       for long optimisation routines.    
    3. Calls to analyt or semi-num functions integrate_time & integrate_space
       assist in calculating the second term (integral term)
    4. Log-likelihood value is returned

    --------------------------------------------------------------------------

    cif_parloop, cif_parloop_eas, cif_iso

    These functions calculate the conditional intensity function.

    The conditional intensity function is evaluated at the times, locations &
    magnitudes of a set of specified target earthquakes (j) in target_catalog.

    Seperate functions are used for efficiency/speed. Both cif_parloop and
    cif_parloop_eas call cif_iso for identical parts of the calculation.
    cif_parloop(_eas) contain the calculation required, and a dictionary of 
    arguments passed to this function is frozen by partial function application 
    allowing the function to be mapped in parallel to the correct index range. 
    This is much faster than a calculation intensive for loop.

    This modular approach also allows support for other anisotropic kernels 
    to be added with minimal effort.

    Format of catalog is zmap:
        lon lat decimalyr mo dy mag depth hr min sec

------------------------------------------------------------------------------
------------------------------------------------------------------------------

config.py

This file contains dictionaries of the run options, parameters, input file
locations, and settings relevant to the normal usage of the program.

The second half of the file contains the internally used function 
config.get_param. Upper and lower bounds on the parameters as well as initial
estimates are defined here for use in optimisation routines. 
Take care when editing.

The first half of the file contains *all* relevant options. The default values
are preserved here:

##############################################################################
kernel_options = {'time_kernels': ['original', 'Kagan', 'Kagan_general'],
                  'spatial_kernels': ['gs', 'gs_sc_Ogata1', 'gs_sc_WHJK', 
				      'gs_sc_Kagan', 'pl', 'pl_sc_Ogata', 
				      'pl_sc_WHJK', 'pl_sc_Zhuang'],
                  'anisotropic_kernels': ['none', 'EAS']}

run_options = {'method': 'semi-num--space',
               'opt_switch': 'on',
               'op_code': 'L-BFGS-B',
               'random_init_params': 0,
               'results_file': 'auto',
               'record_config': True,
               'verbose': 0
               }

main_param = {'start_learning': 1985,
              'mc_learning': 3.95,
              'start_time': [1992, 1, 1, 0, 0, 0, 0], #y,m,d,(h,m,s,ms)
              'end_time': [2012, 1, 1, 0, 0, 0, 0],
              'mc_target': 3.95,
              'test_boundary_dist': 1}

anis_param = {'min_mag_anis': 5.5,
              'start_ea_collect': 1.0/24.0, #Make sure this is float
              'end_ea_collect': 2.0,
              'max_dist_ea_collect': 2.0, #note units of rupture length
              'max_dist_anis': 3.0,
              'smoothing_sigma': 2.0,
              'anis_q': 1.5, # exponent for power-law smoothing
              'anis_d': 2.0} # scale for power-law smoothing

input_settings = {'input_catalog': 'INPUT/input_catalog.npy',
                  'spatial_test_bins': 'INPUT/SpatialTestBins.npy',
                  'collection_area': 'INPUT/RELMCollectionArea.npy',
                  'spatial_dens_bg': 'INPUT/CIF_conan_m2M4_km2.npy',
                  'b' : 1.0,
                  'param_file': 'INPUT/param_original_gs_none.npy'}
              
optimisation_options = {'disp': 0,
                        'maxiter': 15000,
                        'maxfun': 15000,
                        'ftol': 2.22e-9,
                        'gtol': 1e-10
                        }
##############################################################################

kernel_options:
--------------

All combinations of kernel selected are run

time_kernels:     
original      - Original Omori Law 
Kagan         - Using Kagan's parameterisation of the c value
Kagan_general - Generalised version of Kagan's parameterised Omori Law

spatial_kernels:
gs           - Simple gaussian kernel
               Model 1 in Zhuang et al 2011, Rathbun 1993, Console et al 2003
               Uses sigma from Console et al 2003 (3.85 km)

gs_sc_Ogata1 - Ogata 1998, Zhuang et al. 2002
               (scales variance by exp(alpha^(m-mc)))
               (Zhuang et al 2011 Model 2)
               Zhuang et al 2011: d = 0.001 per deg^2 | 0.00004 * (111**2)

gs_sc_WHJK   - After Werner et al 2011, eqn 18

gs_sc_Kagan  - After Kagan 1991, scaling the variance

pl           - Simple power-law kernel from Ogata 1998 (Form 2)
               Console et al 2003, Model 3 in Zhuang et al (2011)

pl_sc_Ogata  - Scaled power-law kernel from Ogata (1998) Form 3, 
               Zhuang et al. (2011) Model 4
    
pl_sc_WHJK   - Scaled power-law kernel from Werner et al, 2011.

pl_sc_Zhuang - Scaling from Ogata & Zhuang (2006) 
               Model 5 in Zhuang et al 2011
               Values from Zhuang et al. (2008) SoCal study

anisotropic_kernels: 
none 
EAS - Early Aftershock Smoothing

------------------------------------------------------------------------------

run_options:
-----------

method       - method used for integration of the cif for second term in
               the log-likelihood calculation 
               'analyt', 'semi-num--(time, space, all)'

opt_switch   - 'on' for optimisation/parameter estimisation
               'off' requires a .npy file containing the appropriate ETAS 
               model parameters (filename can be specified in input_settings)
               'test' runs the calculation on the estimated parameters

op_code       - choose the solver to use
                'L-BFGS-B' is recommended. Should generally leave as default.
                This option mainly exists so it can be used to later add 
                support for different optimisation packages with minimal 
                changes to the body of the code and without 
                building in unnecessary functionality.

random-init-params - Generate random initial parameters between 
                upper/lower bounds?

results_file  - Leave as 'auto' for time stamp format

record_config - Boolean option to record the config.py options

log_param     - DEPRECATED natural logs of the parameters in the optimisation
                routine appear to affect of maximisation of the likelihood

verbose -       Set to '1' for runtime information on integrations, '2' for 
                convergence information

------------------------------------------------------------------------------

main_param:
----------

start_learning - Start year cut-off of learning catalog

mc_learning    - Magnitude threshold of learning catalog

start_time     - Start time cut-off for target earthquakes

end_time       - End time cut-off for target earthquakes

mc_target      - Lower magnitude threshold of target earthquakes

test_boundary_dist - Applies to semi-num methods only. When set to 1 spatial 
                     integration will only be done when earthquake distance 
                     from the region boundary > an exclusion limit.
                     The exclusion limits are defined in semi-num.py:
                     gaussian kernels:  exclusion_limit = 2 * 5.92 * spat_sc
                     power-law kernels: exclusion_limit = 100 * spat_sc

------------------------------------------------------------------------------

anis_param:
----------

min_mag_anis        - Calculate anisotropic kernel when m >= min_mag_anis

start_ea_collect    - Start point for collection of aftershocks to smooth

end_ea_collect      - End point for collection of aftershocks to smooth

max_dist_ea_collect - Collect aftershocks within max_dist_ea_collect

max_dist_anis       - Calculate anisotropic kernel when dist < max_dist_anis

smoothing_sigma     - Standard deviation for gaussian smoothing

anis_q              - Exponent for power-law smoothing

anis_d              - Scaling for power-law smoothing

------------------------------------------------------------------------------

Input settings:
--------------

All input files should be in .npy format
Matlab data can be loaded with scipy.io.loadmat however this is slow - 
conversion is heavily recommended to avoid lengthy overheads

input_catalog     - Raw earthquake catalogue data in zmap format

spatial_test_bins - Pre-split 0.1x0.1 degree spatial bins of testing area

collection_area   - Earthquake collection region

spatial_dens_bg   - Background spatial density - the normalised probability 
                    divided by the area (in km2_ of each spatial cell

b                 - 

param_file        - Required if 'opt_switch' set to 'off'
                    See etas.ll_ETAS documentation for expected format

------------------------------------------------------------------------------

optimisation_options:
--------------------

Required options for scipy.optimize solvers
It is recommended to leave as deafult values

ftol & gtol are the useful options, they effect solver error. However runtime
is greatly affected for little gain beyond default values

disp    - Sets solver verbosity. Avoid, use verbose above instead
maxiter - Maximum iterations, default unlikely to be reached
maxfun  - Maximum number of function evaluations, unlikely to be reached
ftol    - Solver exits when:
          (f^k - f^{k+1})/max{|f^k|,|f^{k+1}|,1} <= ftol
	  2.22e-9 = 1e7 * machine epsilon (2.22e-16)
gtol    - Iteration stops when max{|proj g_i | i = 1, ..., n} <= gtol 
          where pg_i is the i-th component of the projected gradient.

------------------------------------------------------------------------------
------------------------------------------------------------------------------

calc.py

Contains simple calculations and function evaluations

    calc.time_kr

    Evaluates the Omori Law

    c parameter used in calculation of time_kr varies with kernel used:

    'original':
        Evaluates the Omori-Law pdf at values u for given parameters c, p:
        f(u) = (1-p) c^(1-p) /(u+c)^p
        where u = t_j - t_i >=0.
        requires p>1 for normalization.

    'Kagan':
        Evaluates the Omori law using Kagan's parameterization of
        the c-value. c = c_r * 10^([m+6]/2)

    'Kagan_general':
        Evaluates the Omori law using a generalized Kagan's parameterization
        of the c-value. c = c_0 * 10^(chi*m)
    
    --------------------------------------------------------------------------

    calc.spat_kr

    Evaluates the spatial triggering kernel, here assumed to be a
    two-dimensional, isotropic normal density with standard deviation sigma
    
    --------------------------------------------------------------------------

    calc.rupture_length

    Calculate rupture length of an earthquake of magnitude mag

    --------------------------------------------------------------------------

    calc.cif_bg

    Returns the likelihood of the locations vX, vY [lat, lon],
    as epicenters from a background model (read from bg_input_file).
    
    --------------------------------------------------------------------------

    calc.testing_region_polygon

    For the specified forecast, generate a GMT-ready outline of the testing
    region covered by the forecast.  To do this, we find the min/max
    latitude, and at each latitude point in this range, find the min/max
    longitude point.  We then collect these lat/lon points in a format that
    is ready for GMT plotting.

    Argument:

        region_bins - must be an nx4 numpy array in the format:
                      lonmin lonmax latmin latmax

    Returns:
        polygon_points - numpy array of points representing border of the
                         region covered by 'region_bins'

    Original Matlab Code - Copyright (C) 2008 by J. Douglas Zechar
 
    --------------------------------------------------------------------------

    calc.aic/aic_c/bic

    calc.aic returns the Akaike information criterion (AIC)
    This is a measure of the relative quality of statistical models for a 
    given set of data. It penalises models with more parameters (and hence 
    more uncertainty)
    
    calc.aic_c returns the corrected AIC

    calc.bic returns the Bayesian information criterion, BIC.
    A lower BIC is preferred.
    The principle is that when fitting models it is possible to increase the
    likelihood by adding parameters, but doing so may result in overfitting,
    so this measure penalises models with more parameters

------------------------------------------------------------------------------
------------------------------------------------------------------------------

analyt.py

Calculate the integral terms of the log-likelihood equation.

These functions are able to integrate analytically any combination of 
the following spatial/temporal kernels:
   
    temporal: ['original', 'Kagan', 'Kagan_general']
    spatial:  ['gs', 'gs_sc_Ogata1', 'gs_sc_WHJK', 
               'gs_sc_Kagan', 'pl', 'pl_sc_Ogata', 
               'pl_sc_WHJK', 'pl_sc_Zhuang']
                 
This provides a comparison benchmark to test the feasibility/accuracy
of numerical integration methods which can potentially be used on models
where analytical methods fail.

------------------------------------------------------------------------------
------------------------------------------------------------------------------

semi-num.py

    semi-num.integrate_time
    
    Calculates the integral of Omori's law over the period from the 
    start_time to end_time given eq start time v_times for eq v_mag.

    Uses a parallel structure to integrate for earthquakes simultaneously.
    
    semi-num.return_time_integral is used as a holder to map the integration 
    to a partial function so map() can be used.
    
    semi-num.return_time_integral uses scipy.integrate.quad to perform the
    integration on the integrand function contained in semi-num.omori_func

    --------------------------------------------------------------------------

    semi-num.integrate_space

    Calculates the integral of the spatial kernel, 'spat_kr', centred at the 
    earthquake locations v_lat/v_lon over the test volume specified by the
    CSEP testing region.
    
    The approach taken is to integrate discretely cell by cell

    v_m is also passed to this function as the bandwidth (scale factor) of the
    kernel may scale with magnitude (model/kernel dependent)

    Integrand functions for gaussian and power-law kernels are contained in
    semi-num.gs_func and semi-num.pl_func respectively

    Memory problems associated with the large arrays and the cell-by-cell
    technique prevent parallelisation here. 

    The method used is very inefficient in terms of memory and calculations
    performed. An alternative could be the method of Lippiello et al. (2014)

------------------------------------------------------------------------------
------------------------------------------------------------------------------

distance.py

Calculates distance between two points on Earth

Functions:

   gc_distance - Calculates great circle distances assuming a sphere 
                 of radius 6371.01 km.

   v_distance  - Applies Vincenty's algorithms for geodesic distances to the 
                 Earth modelled as the using the WGS84 ellipsoid.

Latitude & Longitude inputs are assumed in degrees and can be single 
values, numpy arrays or a combination of both.

Default output is in kilometres and is distance only. Specify nargout for
azimuth values to be returned also.

See module docstrings for futher information/example usage.
 
------------------------------------------------------------------------------
------------------------------------------------------------------------------
  
timing.py

Used to timestamp operation throughout a run using class timing.Timer

    Creating an instance of Timer starts the timer and print a 
    'Start Program' message. 

    timer.split prints a current timestamp of overall with optional message.
    Lap time since a previous call is also shown if not the first call.

    timer.end displays total run-time with an 'End Program' statement

See module documentation for more information and usage syntax

------------------------------------------------------------------------------
------------------------------------------------------------------------------
==============================================================================


REFERENCES
==========

ETAS Model/Conditional Log-likelihood Function, Omori Law & Spatial Kernels:
---------------------------------------------------------------------------

Console, R., Murru, M., Lombardi, A.M., 2003. Refining earthquake clustering 
   models. J. Geophys. Res. 108, 2468. doi:10.1029/2002JB002130.

Daley, D. J., and D. Vere-Jones (2003). An Introduction to the Theory of Point 
   Processes, vol. I, Springer, New York, USA.

Hawkes, A. G. (1971). Spectra of some self-exciting and mutually exciting 
   point processes, Biometrika, 58 (1), 83–90, doi:10.1093/biomet/58.1.83.

Hawkes, A. G., and D. Oakes (1974), A cluster process representation of a 
   self-exciting process, J. of Appl. Prob., 11 (3), 493–503.

Helmstetter, A., Y. Y. Kagan, and D. D. Jackson (2006). Comparison of short-
   term and timeindependent earthquake forecast models for southern 
   California, Bull. Seismol. Soc. Am., 96 (1), doi:10.1785/0120050067.

Kagan, Y. Y. (1991), Likelihood analysis of earthquake catalogs. 
   Geophys. J. Intern., 106, 135–148.

Ogata, Y. (1988). Statistical models for earthquake occurrence and residual 
   analysis for point processes, J. Am. Stat. Assoc., 83, 9–27.

Ogata, Y. (1998). Space-time point-process models for earthquake occurrences, 
   Ann. Inst. Stat. Math., 5 (2), 379–402.

Omori, F., On after-shocks (1894a). Rep. Imp. Earthq. Inv. Corn., 2, 
   103-138. (in Japanese).

Omori, F., On after-shocks of earthquakes (1894b). 
   J. Coll. Sci. Imp. Univ. Tokyo, 7, 111-200. 

Rathbun, S.L., (1993). Modeling marked spatio-temporal point patterns. 
   Bull. Int. Statist. Inst. 55 (Book 2), 379 – 396.

Utsu, T. (1961). A statistical study of the occurrence of aftershocks. 
   Geophysical Magazine 30: 521–605.

Utsu, T.; Ogata, Y.; Matsu'ura, R.S. (1995). The centenary of the Omori 
   formula for a decay law of aftershock activity. 
   Journal of Physics of the Earth 43: 1–33.

Werner, M. J., A. Helmstetter, D. D. Jackson, and Y. Y. Kagan (2011). 
   High resolution longterm and short-term earthquake forecasts for California, 
   Bull. Seismol. Soc. Am., 101 (4), doi:10.1785/0120090340.

Zhuang, J., Christophersen, A., Savage, M. K., Vere-Jones, D., Ogata, Y., and 
   Jackson, D, D., (2008). Differences between spontaneous and triggered 
   earthquakes: Their influences on foreshock probabilities, J. Geophys. Res. 
   Solid Earth 113, B11302, doi 10.1029/2008JB005579.

Zhuang, J., and Ogata, Y., (2006). Properties of the probability distribution 
   associated with the largest event in an earthquake cluster and 
   their implications to foreshocks, Physical Review, E, 73, 046,134, 
   doi:10.1103/PhysRevE.73.046134.

Zhuang, J., Ogata, Y., Vere-Jones, D., (2002). Stochastic declustering of 
   space–time earthquake occurrences. J. Am. Stat. Assoc. 97, 369–380.

Zhuang, J., Ogata, Y., and Vere-Jones, D., (2004). Analyzing earthquake 
   clustering features by using stochastic reconstruction, J. Geophys. 
   Res., 109 (B05301), doi:10.1029/2003JB002879.

Zhuang, J., Werner, M., Hainzl, S., Harte, D., and Zhou, S., (2011). Basic models 
   of seismicity: spatiotemporal models, Community Online Resource for 
   Statistical Seismicity Analysis, doi:10.5078/corssa-07487583.


Anisotropy/EAS Kernel:
---------------------

Bach, C., and S. Hainzl (2012). Improving empirical aftershock modeling based 
   on additional source information, J. of Geophys. Res., 117 (B4), B04,312.

Helmstetter, A., Kagan, Y. Y., and Jackson, D. D., (2006). Comparison of short-
   term and timeindependent earthquake forecast models for southern 
   California, Bull. Seismol. Soc. Am., 96 (1), doi:10.1785/0120050067.

Kagan, Y. Y. (2007). Simplified algorithms for calculating double-couple 
   rotation, Geophysical Journal International, 171 (1), 411–418, 
   doi:10.1111/j.1365-246X.2007.03538.x.


Computational Algorithms & Packages:
-----------------------------------

Byrd, R. H., Lu, P., and Nocedal, J., (1995). A Limited Memory Algorithm for 
   Bound Constrained Optimization, SIAM Journal on Scientific and Statistical 
   Computing, 16, 5, pp. 1190-1208.

Hold-Geoffroy, Y., Gagnon, O. and Parizeau, M., (2014). 
   Once you SCOOP, no need to fork. 
   In Proceedings of the 2014 Annual Conference on Extreme Science and 
   Engineering Discovery Environment (p. 60). ACM.

Hunter, J. D., (2007). Matplotlib: A 2D graphics environment. Computing In
   Science & Engineering, 9 (3), 90-95.

Morales, J. L. and Nocedal, J., (2011). L-BFGS-B: Remark on Algorithm 778: 
   L-BFGS-B, FORTRAN routines for large scale bound constrained optimization, 
   ACM Transactions on Mathematical Software, 38, 1.

Stéfan van der Walt, S. Chris Colbert and Gaël Varoquaux. (2011) 
   The NumPy Array: A Structure for Efficient Numerical Computation, 
   Computing in Science & Engineering, 13, 22-30.

Vincenty, T., (1975). Direct and Inverse Solutions of Geodesics on the 
   Ellipsoid with application of nested equations, Survey Review, 
   vol XXIII no 176, doi:10.1179/sre.1975.23.176.88,
   http://www.ngs.noaa.gov/PUBS_LIB/inverse.pdf

Zhu, C., Byrd, R. H., and Nocedal, J., (1997). L-BFGS-B: Algorithm 778: 
   L-BFGS-B, FORTRAN routines for large scale bound constrained optimization, 
   ACM Transactions on Mathematical Software, 23, 4, pp. 550 - 560.

------------------------------------------------------------------------------
This work was carried out using the computational facilities of the Advanced 
Computing Research Centre, University of Bristol - http://www.bris.ac.uk/acrc/
------------------------------------------------------------------------------


Further Reading:
===============


Interpetation, Significance & Scaling of 'c' Value:
--------------------------------------------------

Kagan, Y. Y. (2004). Short-term properties of earthquake catalogs and models 
   of earthquake source. Bull. Seismol. Soc. Am., 94 (4), 1207–1228.

Kagan, Y. Y., and H. Houston (2005). Relation between mainshock rupture 
   process and omori’s law for aftershock moment release rate.
   Geophys. J. Int., 163, 1039–1048, doi:10.1111/j.1365-246X.2005.02772.x.

Shcherbakov, R., D. L. Turcotte, and J. Rundle (2004). A generalized omori’s 
   law for earthquake aftershock decay, Geophys. Res. Lett., 31 (L11613). 
   doi:10.1029/2004GL019808.

Shebalin, P., C. Narteau, M. Holschneider, and D. Schorlemmer (2011). 
   Short-term earthquake forecasting using early aftershock statistics.
   Bulletin of the Seismological Society of America, 101 (1), 297–312.


Missing Aftershocks:
-------------------

Helmstetter, A., Y. Y. Kagan, and D. D. Jackson (2006), Comparison of 
   short-term and time-independent earthquake forecast models for southern 
   California, Bull. Seismol. Soc. Am., 96 (1). doi:10.1785/0120050067.

Kagan, Y. Y. (1991), Likelihood analysis of earthquake catalogs. 
   Geophys. J. Intern., 106, 135–148.

Kagan, Y. Y. (2004), Short-term properties of earthquake catalogs and models 
   of earthquake source, Bull. Seismol. Soc. Am., 94 (4), 1207–1228.

Lennartz, S., A. Bunde, and D. L. Turcotte (2008), Missing data in aftershock 
   sequences: Explaining the deviations from scaling laws, Physical Review E 
   (Statistical, Nonlinear, and Soft Matter Physics), 78 (4), 041115. 
   doi:10.1103/PhysRevE.78.041115.

Ogata, Y., and K. Katsura (2006). Immediate and updated forecasting of
   aftershock hazard, Geophys. Res. Lett., 33, 10,305-+. 
   doi:10.1029/2006GL025888.

Peng, Z., J. E. Vidale, M. Ishii, and A. Helmstetter (2007). Seismicity rate 
   immediately before and after main shock rupture from high-frequency 
   waveforms in Japan, J. Geophys. Res., 112 (B03306) doi:10.1029/2006JB004386

Werner, M. J., A. Helmstetter, D. D. Jackson, and Y. Y. Kagan (2011). 
   High resolution long-term and short-term earthquake forecasts for 
   California, Bull. Seismol. Soc. Am., 101 (4). doi:10.1785/0120090340.


CBM (Critical Branching Models):
-------------------------------

Kagan, Y. Y., and Jackson, D. D., (1994). Long-term probabilistic forecasting of 
   earthquakes, J. Geophys. Res., 99 (B7), 13,685–13,700.

Kagan, Y. Y., and Jackson, D. D., (2010). Short- and long-term earthquake
   forecasts for california and nevada, Pure and Appl. Geophys.: The Frank 
   Evison Volume, 167 (6/7), 685–692, doi:10.1007/s00024-010-0073-5.

Kagan, Y. Y., and Knopoff, L., (1987). Statistical short-term earthquake
   prediction, Science, 236 (4808), 1563–1567.


Alternative Methods for Anisotropy:
----------------------------------

Kagan, Y. Y., and Jackson, D. D., (2000). Probabilistic forecasting of 
   earthquakes, Geophys. J. Int., 143, 483–453.

Ogata, Y., and Zhuang, J., (2006). Space-time ETAS models and an improved 
   extension, Tectonophysics, 413, 13–23.

